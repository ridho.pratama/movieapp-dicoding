package com.application.idong.moviecatalogapps.ui.movies

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.application.idong.moviecatalogapps.MainCoroutineRule
import com.application.idong.moviecatalogapps.data.MovieRepository
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote
import com.application.idong.moviecatalogapps.utils.DataMoviesRemote
import com.application.idong.moviecatalogapps.vo.Resource
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by ridhopratama on 11,December,2020
 */

@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class MoviesViewModelTest {

    @get:Rule var mainCoroutineRule = MainCoroutineRule()

    @get:Rule var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var movieRepository: MovieRepository

    @Mock
    private lateinit var observerList: Observer<Resource<List<MoviesRemote>>>

    @Mock
    private lateinit var observerDetail: Observer<Resource<MoviesRemote>>

    private lateinit var viewModel: MoviesViewModel

    @Before
    fun setUp() {
        viewModel = MoviesViewModel(movieRepository)
    }

    @Test
    fun `get data movies success, not null and size is 10`() {
        mainCoroutineRule.runBlockingTest {

            //given
            val dummyMovies  = DataMoviesRemote.movies()
            `when`(movieRepository.getRemoteMovies()).thenReturn(dummyMovies)

            //when
            viewModel.getMovies(0)
            verify(movieRepository).getRemoteMovies()

            //then
            val movieViewState = viewModel.moviewViewState.value
            val movieResult = movieViewState?.data as List<MoviesRemote>
            assertNotNull(movieResult)
            assertEquals(10, movieResult.size)
            viewModel.moviewViewState.observeForever(observerList)
            Mockito.verify(observerList).onChanged(movieViewState)
        }
    }

    @Test()
    @Throws(Exception::class)
    fun `get data movies failed`() {
        mainCoroutineRule.runBlockingTest {

            val errorMessage = "Terjadi kesalahan saat melakukan request data"

            //given
            doAnswer { throw Exception(errorMessage) }
                    .whenever(movieRepository)
                    .getRemoteMovies()

            //when
            viewModel.getMovies(0)
            verify(movieRepository).getRemoteMovies()

            //then
            val movieViewState = viewModel.moviewViewState.value
            val movieResult = movieViewState?.data
            assertNull(movieResult)
            assertEquals(errorMessage, movieViewState?.message)
            viewModel.moviewViewState.observeForever(observerList)
            Mockito.verify(observerList).onChanged(movieViewState)
        }
    }

    @Test
    fun `get data tv show success, not null and size is 10`() {
        mainCoroutineRule.runBlockingTest {

            //given
            val dummyMovies  = DataMoviesRemote.tvshow()
            `when`(movieRepository.getRemoteTvshow()).thenReturn(dummyMovies)

            //when
            viewModel.getMovies(1)
            verify(movieRepository).getRemoteTvshow()

            //then
            val movieViewState = viewModel.moviewViewState.value
            val movieResult = movieViewState?.data as List<MoviesRemote>
            assertNotNull(movieResult)
            assertEquals(10, movieResult.size)
            viewModel.moviewViewState.observeForever(observerList)
            Mockito.verify(observerList).onChanged(movieViewState)
        }
    }

    @Test()
    @Throws(Exception::class)
    fun `get data tvshow failed`() {
        mainCoroutineRule.runBlockingTest {

            val errorMessage = "Terjadi kesalahan saat melakukan request data"

            //given
            doAnswer { throw Exception(errorMessage) }
                    .whenever(movieRepository)
                    .getRemoteTvshow()

            //when
            viewModel.getMovies(1)
            verify(movieRepository).getRemoteTvshow()

            //then
            val movieViewState = viewModel.moviewViewState.value
            val movieResult = movieViewState?.data
            assertNull(movieResult)
            assertEquals(errorMessage, movieViewState?.message)
            viewModel.moviewViewState.observeForever(observerList)
            Mockito.verify(observerList).onChanged(movieViewState)
        }
    }

    @Test()
    fun `detail movies category`() {
        mainCoroutineRule.runBlockingTest {

            //given
            val dummyMovies = DataMoviesRemote.movies()[0]
            val idMovies = dummyMovies.id
            `when`(movieRepository.getRemoteMoviesDetail(idMovies)).thenReturn(dummyMovies)

            //when
            viewModel.getDetailMovies(0, idMovies)
            verify(movieRepository).getRemoteMoviesDetail(idMovies)

            //then
            val movieViewState = viewModel.movieDetailViewState.value
            val realMovies = movieViewState?.data as MoviesRemote
            assertNotNull(realMovies)
            assertEquals(dummyMovies, realMovies)

            viewModel.movieDetailViewState.observeForever(observerDetail)
            verify(observerDetail).onChanged(movieViewState)
        }
    }

    @Test()
    fun `detail tvshow category`() {
        mainCoroutineRule.runBlockingTest {

            //given
            val dummyMovies = DataMoviesRemote.movies()[1]
            val idMovies = dummyMovies.id
            `when`(movieRepository.getRemoteTvshowDetail(idMovies)).thenReturn(dummyMovies)

            //when
            viewModel.getDetailMovies(1, idMovies)
            verify(movieRepository).getRemoteTvshowDetail(idMovies)

            //then
            val movieViewState = viewModel.movieDetailViewState.value
            val realMovies = movieViewState?.data as MoviesRemote
            assertNotNull(realMovies)
            assertEquals(dummyMovies, realMovies)

            viewModel.movieDetailViewState.observeForever(observerDetail)
            verify(observerDetail).onChanged(movieViewState)
        }
    }

    @Test()
    fun `detail popular category`() {
        mainCoroutineRule.runBlockingTest {

            //given
            val dummyMovies = DataMoviesRemote.movies()[2]
            val idMovies = dummyMovies.id
            `when`(movieRepository.getRemotePopularDetail(idMovies)).thenReturn(dummyMovies)

            //when
            viewModel.getDetailMovies(2, idMovies)
            verify(movieRepository).getRemotePopularDetail(idMovies)

            //then
            val movieViewState = viewModel.movieDetailViewState.value
            val realMovies = movieViewState?.data as MoviesRemote
            assertNotNull(realMovies)
            assertEquals(dummyMovies, realMovies)

            viewModel.movieDetailViewState.observeForever(observerDetail)
            verify(observerDetail).onChanged(movieViewState)
        }
    }

    @Test()
    fun `detail newest category`() {
        mainCoroutineRule.runBlockingTest {

            //given
            val dummyMovies = DataMoviesRemote.movies()[3]
            val idMovies = dummyMovies.id
            `when`(movieRepository.getRemoteNewestDetail(idMovies)).thenReturn(dummyMovies)

            //when
            viewModel.getDetailMovies(3, idMovies)
            verify(movieRepository).getRemoteNewestDetail(idMovies)

            //then
            val movieViewState = viewModel.movieDetailViewState.value
            val realMovies = movieViewState?.data as MoviesRemote
            assertNotNull(realMovies)
            assertEquals(dummyMovies, realMovies)

            viewModel.movieDetailViewState.observeForever(observerDetail)
            verify(observerDetail).onChanged(movieViewState)
        }
    }

    @Test()
    fun `detail saved category`() {
        mainCoroutineRule.runBlockingTest {

            //given
            val dummyMovies = DataMoviesRemote.movies()[4]
            val idMovies = dummyMovies.id
            `when`(movieRepository.getRemoteSavedDetail(idMovies)).thenReturn(dummyMovies)

            //when
            viewModel.getDetailMovies(4, idMovies)
            verify(movieRepository).getRemoteSavedDetail(idMovies)

            //then
            val movieViewState = viewModel.movieDetailViewState.value
            val realMovies = movieViewState?.data as MoviesRemote
            assertNotNull(realMovies)
            assertEquals(dummyMovies, realMovies)

            viewModel.movieDetailViewState.observeForever(observerDetail)
            verify(observerDetail).onChanged(movieViewState)
        }
    }
}