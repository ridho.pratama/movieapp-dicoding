package com.application.idong.moviecatalogapps.ui.account

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

/**
 * Created by ridhopratama on 11,December,2020
 */
class AccountViewModelTest {

    private lateinit var viewModel: AccountViewModel

    @Before
    fun setUp() {
        viewModel = AccountViewModel()
    }

    @Test
    fun getAccountMenu() {
        val accountMenu = viewModel.getAccountMenu()
        assertNotNull(accountMenu)
        assertEquals(3, accountMenu.size)
    }
}