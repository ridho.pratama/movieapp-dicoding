package com.application.idong.moviecatalogapps.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.application.idong.moviecatalogapps.MainCoroutineRule
import com.application.idong.moviecatalogapps.data.MovieRepository
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote
import com.application.idong.moviecatalogapps.utils.DataMoviesRemote
import com.application.idong.moviecatalogapps.vo.Resource
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by ridhopratama on 11,December,2020
 */
@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class HomeViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var movieRepository: MovieRepository

    @Mock
    private lateinit var observer: Observer<Resource<List<MoviesRemote>>>

    private lateinit var viewModel: HomeViewModel

    @Before
    fun setUp() {
        viewModel = HomeViewModel(movieRepository)
    }

    @Test
    fun `get data popular success, not null and size is 5`() {
        mainCoroutineRule.runBlockingTest {

            //given
            val dummyMovies  = DataMoviesRemote.popular()
            `when`(movieRepository.getRemotePopular()).thenReturn(dummyMovies)

            //when
            viewModel.getPopularMovies()
            verify(movieRepository).getRemotePopular()

            //then
            val movieViewState = viewModel.popularViewState.value
            val movieResult = movieViewState?.data as List<MoviesRemote>
            assertNotNull(movieResult)
            assertEquals(5, movieResult.size)
            viewModel.popularViewState.observeForever(observer)
            verify(observer).onChanged(movieViewState)
        }
    }

    @Test
    fun `get data newest success, not null and size is 5`() {
        mainCoroutineRule.runBlockingTest {

            //given
            val dummyMovies  = DataMoviesRemote.newest()
            `when`(movieRepository.getRemoteNewest()).thenReturn(dummyMovies)

            //when
            viewModel.getNewestMovies()
            verify(movieRepository).getRemoteNewest()

            //then
            val movieViewState = viewModel.newestViewState.value
            val movieResult = movieViewState?.data as List<MoviesRemote>
            assertNotNull(movieResult)
            assertEquals(5, movieResult.size)
            viewModel.newestViewState.observeForever(observer)
            verify(observer).onChanged(movieViewState)
        }
    }
}