package com.application.idong.moviecatalogapps.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.DataSource
import com.application.idong.moviecatalogapps.MainCoroutineRule
import com.application.idong.moviecatalogapps.data.local.LocalDataSource
import com.application.idong.moviecatalogapps.data.local.entity.Movies
import com.application.idong.moviecatalogapps.data.remote.RemoteDataSource
import com.application.idong.moviecatalogapps.ui.PagedListUtil
import com.application.idong.moviecatalogapps.utils.DataMovies
import com.application.idong.moviecatalogapps.utils.DataMoviesRemote
import com.application.idong.moviecatalogapps.vo.Resource
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.verify
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by ridhopratama on 22,December,2020
 */

@RunWith(MockitoJUnitRunner.Silent::class)
@ExperimentalCoroutinesApi
class MovieRepositoryTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val remote = mock(RemoteDataSource::class.java)
    private val local = mock(LocalDataSource::class.java)

    private lateinit var repository: FakeMovieRepository

    private val popularResponse = DataMoviesRemote.popular()
    private val popularId = popularResponse[0].id
    private val newestResponse = DataMoviesRemote.newest()
    private val newestId = popularResponse[1].id
    private val moviesResponse = DataMoviesRemote.movies()
    private val moviesId = popularResponse[2].id
    private val tvshowResponse = DataMoviesRemote.tvshow()
    private val tvshowId = popularResponse[3].id
    private val savedResponse = DataMoviesRemote.saved()
    private val savedId = popularResponse[4].id

    @Before
    fun setup() {
        repository = FakeMovieRepository(remote, local)
    }

    @Test
    fun getRemotePopular() {
        mainCoroutineRule.runBlockingTest {

            //given
            doReturn(popularResponse).`when`(remote).getPopular()

            //when
            val popular = repository.getRemotePopular()
            verify(remote).getPopular()

            //then
            assertNotNull(popular)
            assertEquals(popularResponse.size, popular.size)
        }
    }

    @Test
    fun getRemoteNewest() {
        mainCoroutineRule.runBlockingTest {

            //given
            doReturn(newestResponse).`when`(remote).getNewest()

            //when
            val newest = repository.getRemoteNewest()
            verify(remote).getNewest()

            //then
            assertNotNull(newest)
            assertEquals(newestResponse.size, newest.size)
        }
    }

    @Test
    fun getRemoteMovies() {
        mainCoroutineRule.runBlockingTest {

            //given
            doReturn(moviesResponse).`when`(remote).getMovies()

            //when
            val movies = repository.getRemoteMovies()
            verify(remote).getMovies()

            //then
            assertNotNull(movies)
            assertEquals(moviesResponse.size, movies.size)
        }
    }

    @Test
    fun getRemoteTvshow() {
        mainCoroutineRule.runBlockingTest {

            //given
            doReturn(tvshowResponse).`when`(remote).getTvShow()

            //when
            val tvshow = repository.getRemoteTvshow()
            verify(remote).getTvShow()

            //then
            assertNotNull(tvshow)
            assertEquals(tvshowResponse.size, tvshow.size)
        }
    }

    @Test
    fun getLocalSaved() {
        //given
        val category = "Movies"
        val dataSourceFactory = mock(DataSource.Factory::class.java) as DataSource.Factory<Int, Movies>
        `when`(local.getFavoriteMovies(category)).thenReturn(dataSourceFactory)

        //when
        repository.getFavoriteMovies(category)
        val moviesEntities = Resource.Success(PagedListUtil.mockPagedList(DataMovies.saved()))
        verify(local).getFavoriteMovies(category)

        //then
        assertNotNull(moviesEntities)
        assertEquals(savedResponse.size.toLong(), moviesEntities.data?.size?.toLong())
    }

    @Test
    fun getRemotePopularDetail() {
        mainCoroutineRule.runBlockingTest {

            //given
            doReturn(popularResponse[0]).`when`(remote).getPopularDetail(popularId)

            //when
            val popularDetail = repository.getRemotePopularDetail(popularId)
            verify(remote).getPopularDetail(popularId)

            //then
            assertNotNull(popularDetail)
            assertEquals(popularResponse[0], popularDetail)
        }
    }

    @Test
    fun getRemoteNewestDetail() {
        mainCoroutineRule.runBlockingTest {

            //given
            doReturn(newestResponse[1]).`when`(remote).getNewestDetail(newestId)

            //when
            val newestDetail = repository.getRemoteNewestDetail(newestId)
            verify(remote).getNewestDetail(newestId)

            //then
            assertNotNull(newestDetail)
            assertEquals(newestResponse[1], newestDetail)
        }
    }

    @Test
    fun getRemoteMoviesDetail() {
        mainCoroutineRule.runBlockingTest {

            //given
            doReturn(moviesResponse[2]).`when`(remote).getMoviesDetail(moviesId)

            //when
            val moviesDetail = repository.getRemoteMoviesDetail(moviesId)
            verify(remote).getMoviesDetail(moviesId)

            //then
            assertNotNull(moviesDetail)
            assertEquals(moviesResponse[2], moviesDetail)
        }
    }

    @Test
    fun getRemoteTvshowDetail() {
        mainCoroutineRule.runBlockingTest {

            //given
            doReturn(tvshowResponse[3]).`when`(remote).getTvShowDetail(tvshowId)

            //when
            val tvshowDetail = repository.getRemoteTvshowDetail(tvshowId)
            verify(remote).getTvShowDetail(tvshowId)

            //then
            assertNotNull(tvshowDetail)
            assertEquals(tvshowResponse[3], tvshowDetail)
        }
    }

    @Test
    fun getRemoteSavedDetail() {
        mainCoroutineRule.runBlockingTest {

            //given
            doReturn(savedResponse[4]).`when`(remote).getSavedDetail(savedId)

            //when
            val savedDetail = repository.getRemoteSavedDetail(savedId)
            verify(remote).getSavedDetail(savedId)

            //then
            assertNotNull(savedDetail)
            assertEquals(savedResponse[4], savedDetail)
        }
    }
}