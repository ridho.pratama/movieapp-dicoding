package com.application.idong.moviecatalogapps.ui.saved

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import com.application.idong.moviecatalogapps.MainCoroutineRule
import com.application.idong.moviecatalogapps.data.MovieRepository
import com.application.idong.moviecatalogapps.data.local.entity.Movies
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote
import com.application.idong.moviecatalogapps.utils.DataMoviesRemote
import com.application.idong.moviecatalogapps.vo.Resource
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by ridhopratama on 11,December,2020
 */

@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class SavedViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var movieRepository: MovieRepository

    @Mock
    private lateinit var observer: Observer<PagedList<Movies>>

    @Mock
    private lateinit var pagedList: PagedList<Movies>

    private lateinit var viewModel: SavedViewModel

    @Before
    fun setUp() {
        viewModel = SavedViewModel(movieRepository)
    }

    @Test
    fun `get data saved Movies success, not null and size is 10`() {
        //given
        val category = "Movies"
        val dummyMovies  = pagedList
        `when`(dummyMovies.size).thenReturn(10)

        //when
        val movies = MutableLiveData<PagedList<Movies>>()
        movies.value = dummyMovies
        `when`(movieRepository.getFavoriteMovies(category)).thenReturn(movies)
        val moviesEntities = viewModel.getSavedMovies(category).value
        verify(movieRepository).getFavoriteMovies(category)

        //then
        assertNotNull(moviesEntities)
        assertEquals(10, moviesEntities?.size)

        viewModel.getSavedMovies(category).observeForever(observer)
        verify(observer).onChanged(dummyMovies)
    }
}