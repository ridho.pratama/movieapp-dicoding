package com.application.idong.moviecatalogapps.ui.account

import androidx.lifecycle.ViewModel
import com.application.idong.moviecatalogapps.models.AccountMenu
import com.application.idong.moviecatalogapps.utils.Menu

/**
 * Created by ridhopratama on 10,December,2020
 */

class AccountViewModel: ViewModel() {

    fun getAccountMenu(): MutableList<AccountMenu> = Menu.accountMenu()

}