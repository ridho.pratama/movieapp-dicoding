package com.application.idong.moviecatalogapps.api

import com.application.idong.moviecatalogapps.BuildConfig
import okhttp3.*

/**
 * Created by ridhopratama on 20,December,2020
 */

class InterceptorApi : Interceptor, Authenticator {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader(BuildConfig.AUTH_HEADER, BuildConfig.AUTH_KEY)
                .build()

        return chain.proceed(request)
    }

    override fun authenticate(route: Route?, response: Response): Request? {
        return response.request.newBuilder()
            .header(BuildConfig.AUTH_HEADER, BuildConfig.AUTH_KEY)
            .build()
    }
}