package com.application.idong.moviecatalogapps.ui.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.application.idong.moviecatalogapps.api.UriService
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.databinding.ItemsMoviesBinding
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

/**
 * Created by ridhopratama on 06,December,2020
 */
class MoviesAdapter(private val listener: MovieListener): RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {
    private val movies = mutableListOf<MoviesRemote>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemsMovies = ItemsMoviesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemsMovies)
    }

    override fun getItemCount(): Int = movies.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = movies[position]
        holder.bind(data)
    }

    fun updateData(newMovies: MutableList<MoviesRemote>) {
        movies.clear()
        movies.addAll(newMovies)
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemsMoviesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(movie: MoviesRemote) {
            with(binding) {
                var category = ""
                if (movie.category.equals(itemView.context.getString(R.string.title_tvshow), true)) {
                    category = itemView.context.resources.getString(R.string.title_detail_category_tvshow, movie.category, movie.chapter?.size)
                    tvCategory.setBackgroundResource(R.drawable.shape_roundcornerleftbottom_primary)
                } else {
                    category = movie.category
                    tvCategory.setBackgroundResource(R.drawable.shape_roundcornerleftbottom_accent)
                }
                tvCategory.text = category
                tvTitle.text = "${movie.title} (${movie.year})"
                tvGenre.text = movie.genre
                tvTime.text = movie.time
                tvRating.text = movie.rating
                Glide.with(itemView.context)
                    .load(UriService.POSTER_URL + movie.poster)
                    .apply(RequestOptions.placeholderOf(R.drawable.img_loading)
                        .error(R.drawable.img_error))
                    .into(imgPoster)

                itemView.setOnClickListener {
                    listener.selectMovie(movie)
                }
            }
        }
    }
}