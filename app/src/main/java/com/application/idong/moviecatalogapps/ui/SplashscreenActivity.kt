package com.application.idong.moviecatalogapps.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowInsets
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.utils.Constant

/**
 * Created by ridhopratama on 10,December,2020
 */

class SplashscreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        }
        else {
            @Suppress("DEPRECATION")
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        gotoNextActivity()
    }

    private fun gotoNextActivity() {
        Handler(Looper.getMainLooper()).postDelayed({
            openActivityAndClearPrevious()
        }, Constant.DELAY_SPLASHSCREEN)
    }

    private fun openActivityAndClearPrevious() {
        try {
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
        }
        catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}