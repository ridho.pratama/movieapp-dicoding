package com.application.idong.moviecatalogapps.ui.movies

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.application.idong.moviecatalogapps.api.UriService
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.data.local.entity.Movies
import com.application.idong.moviecatalogapps.databinding.ActivityDetailMovieBinding
import com.application.idong.moviecatalogapps.data.remote.entity.ChapterRemote
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote
import com.application.idong.moviecatalogapps.factory.ViewModelFactory
import com.application.idong.moviecatalogapps.ui.saved.SavedViewModel
import com.application.idong.moviecatalogapps.utils.ConvertObject
import com.application.idong.moviecatalogapps.vo.Resource
import com.application.idong.moviecatalogapps.utils.ViewUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.snackbar.Snackbar
import kotlin.math.abs

class DetailMovieActivity : AppCompatActivity(), ChapterListener {

    private lateinit var moviesViewModel: MoviesViewModel
    private lateinit var savedViewModel: SavedViewModel
    private lateinit var adapterCast: CastMoviesAdapter
    private lateinit var adapterChapter: ChapterMoviesAdapter

    companion object {
        const val EXTRA_CATEGORY = "category"
        const val EXTRA_MOVIEID = "movieId"
    }

    private lateinit var binding: ActivityDetailMovieBinding
    private var category: Int = 0
    private var movieId: Int = 0
    private var movies: MoviesRemote? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailMovieBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupToolbar()
        val bundle = intent.extras
        if (bundle != null) {
            category = bundle.getInt(EXTRA_CATEGORY)
            movieId = bundle.getInt(EXTRA_MOVIEID)
        }
        loadCast()
        loadChapter()
        loadData()
    }

    private fun setupToolbar() {
        with(binding.tbPartialCollapsing) {
            btnBack.setOnClickListener {
                onBackPressed()
            }
        }

        with(binding.ablDetailMovie) {
               addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
                   when {
                       abs(verticalOffset) == appBarLayout.totalScrollRange -> {
                           window.statusBarColor = ContextCompat.getColor(this@DetailMovieActivity, R.color.colorWhite)
                       }
                       verticalOffset == 0 -> {
                           window.statusBarColor = ContextCompat.getColor(this@DetailMovieActivity, android.R.color.transparent)
                       }
                       else -> {
                           if (abs(verticalOffset) > appBarLayout.totalScrollRange / 2) {
                               window.statusBarColor = ContextCompat.getColor(this@DetailMovieActivity, R.color.colorWhite)
                               binding.cvActionTop.visibility = View.GONE
                           }
                           else {
                               window.statusBarColor = ContextCompat.getColor(this@DetailMovieActivity, android.R.color.transparent)
                               binding.cvActionTop.visibility = View.VISIBLE
                           }
                       }
                   }
               })
        }
    }

    private fun loadData() {
        val factory = ViewModelFactory.getInstance(this)
        moviesViewModel = ViewModelProvider(this, factory)[MoviesViewModel::class.java].apply {
            movieDetailViewState.observe(this@DetailMovieActivity, Observer(this@DetailMovieActivity::handleState))
            getDetailMovies(category, movieId)
        }
        savedViewModel = ViewModelProvider(this, factory)[SavedViewModel::class.java]
        savedViewModel.getFavoriteMoviesById(movieId).observe(this, { movie ->
            toggleFavorite(movie)
        })

    }

    private fun handleState(viewState: Resource<MoviesRemote>?) {
        viewState?.let { response ->
            when(response) {
                is Resource.Loading -> toggleLoading(true)
                is Resource.Success -> {
                    toggleLoading(false)
                    response.data?.let {
                        showData(it)
                    }
                }
                is Resource.Error -> {
                    toggleLoading(false)
                    response.message?.let {
                        showError(it)
                    }
                }
            }
        }
    }

    private fun  toggleLoading(loading: Boolean) {
        if (loading) {
            binding.loading.visibility = View.VISIBLE
        }
        else {
            binding.loading.visibility = View.GONE
        }
    }

    private fun showData(movies: MoviesRemote) {
        this.movies = movies
        binding.contentDetail.root.visibility = View.VISIBLE

        Glide.with(this)
            .load(UriService.POSTER_URL + movies.poster)
            .apply(
                RequestOptions.placeholderOf(R.drawable.img_loading)
                    .error(R.drawable.img_error))
            .into(binding.ivPoster)
        binding.tvRating.text = movies.rating

        with(binding.contentDetail) {
            var category = ""
            if (movies.category.equals(getString(R.string.title_tvshow), true)) {
                category = resources.getString(R.string.title_detail_category_tvshow, movies.category, movies.chapter?.size )
                tvCategory.setBackgroundResource(R.drawable.shape_roundcorner_primary)
            } else {
                category = movies.category.toString()
                tvCategory.setBackgroundResource(R.drawable.shape_roundcorner_accent)
            }
            tvCategory.text = category
            tvTitleMovie.text = movies.title
            tvYear.text = movies.year
            tvTime.text = movies.time
            tvGenre.text = movies.genre
            tvSummary.text = movies.summary
        }

        adapterCast.updateData(movies.cast.toMutableList())
        if (movies.category.equals(getString(R.string.title_tvshow), true)) {
            binding.contentDetail.llChapter.visibility = View.VISIBLE
            movies.chapter?.let {
                adapterChapter.updateData(it.toMutableList())
            }
        }
    }

    private fun showError(error: String) {
        ViewUtil.showToast(this, error)
    }

    private fun loadCast() {
        adapterCast = CastMoviesAdapter()
        binding.contentDetail.rvCast.apply {
            overScrollMode = View.OVER_SCROLL_NEVER
            layoutManager = LinearLayoutManager(this@DetailMovieActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = adapterCast
            ViewCompat.setNestedScrollingEnabled(this, false)
        }
    }

    private fun loadChapter() {
        adapterChapter = ChapterMoviesAdapter(this)
        binding.contentDetail.rvChapter.apply {
            layoutManager = GridLayoutManager(this@DetailMovieActivity, 4)
            setHasFixedSize(true)
            adapter = adapterChapter
            ViewCompat.setNestedScrollingEnabled(this, false)
        }
    }

    private fun toggleFavorite(movie: Movies?) {
        if (movie == null) {
            binding.tvSaved.text = getString(R.string.title_unsaved_favorite)
            binding.ivSaved.apply {
                setImageResource(R.drawable.ic_bookmark_line)
                setColorFilter(
                        ContextCompat.getColor(this@DetailMovieActivity, R.color.colorAccent),
                        PorterDuff.Mode.SRC_ATOP
                )
            }
            binding.llBookmarkDetail.setOnClickListener {
                setFavoriteMovie()
            }
        }
        else {
            binding.tvSaved.text = getString(R.string.title_saved_favorite)
            binding.ivSaved.apply {
                setImageResource(R.drawable.ic_bookmark)
                setColorFilter(
                        ContextCompat.getColor(this@DetailMovieActivity, R.color.colorAccent),
                        PorterDuff.Mode.SRC_ATOP
                )
            }
            binding.llBookmarkDetail.setOnClickListener {
                deleteFavorite(movie)
            }
        }
    }

    private fun setFavoriteMovie() {
        lateinit var movie: Movies
        this.movies?.let { movieRemote ->
            movie = Movies(
                    movieRemote.id,
                    movieRemote.title,
                    movieRemote.category,
                    movieRemote.genre,
                    movieRemote.time,
                    movieRemote.rating,
                    movieRemote.year,
                    movieRemote.poster,
                    movieRemote.summary,
                    ConvertObject.collectionCastRemoteToCast(movieRemote.cast),
                    ConvertObject.collectionChapterRemoteToChapter(movieRemote.chapter)
            )
        }

        if (movie != null) {
            savedViewModel.upsert(movie)
            Snackbar.make(binding.root, R.string.title_success_saved_favorite, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun deleteFavorite(movie: Movies) {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle("Delete favorite movie")
        alertDialogBuilder
                .setMessage(getString(R.string.title_delete_favorite_confirm))
                .setCancelable(false)
                .setPositiveButton("Yes") { dialog, id ->
                    savedViewModel.delete(movie)
                }
                .setNegativeButton("No") {
                    dialog, id -> dialog.cancel()
                }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }

    override fun onSelectChapter(chapter: ChapterRemote) {
        with(binding.contentDetail) {
            tvTitleMovie.text = "${movies?.title} - ${chapter.chapterName}"
            tvYear.text = chapter.chapterYear
        }
    }
}