package com.application.idong.moviecatalogapps.data.local.database

import androidx.room.TypeConverter
import com.application.idong.moviecatalogapps.data.local.entity.Cast
import com.application.idong.moviecatalogapps.data.local.entity.Chapter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

/**
 * Created by ridhopratama on 02,January,2021
 */
class RoomConverter {

    @TypeConverter
    fun fromListCast(value: List<Cast>) : String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun toListCast(value: String) : List<Cast> {
        val listType: Type = object : TypeToken<List<Cast>>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromListChapter(value: List<Chapter>?) : String? {
        if (value == null)
            return null

        return Gson().toJson(value)
    }

    @TypeConverter
    fun toListChapter(value: String?) : List<Chapter>? {
        if (value == null)
            return null

        val listType: Type = object : TypeToken<List<Chapter>>() {}.type
        return Gson().fromJson(value, listType)
    }
}