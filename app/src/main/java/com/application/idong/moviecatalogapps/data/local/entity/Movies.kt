package com.application.idong.moviecatalogapps.data.local.entity

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by ridhopratama on 07,December,2020
 */
@Entity(tableName = "movies")
data class Movies(
        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "moviesId")
        var moviesId: Int,

        @ColumnInfo(name = "title")
        var title: String,

        @ColumnInfo(name = "category")
        var category: String,

        @ColumnInfo(name = "genre")
        var genre: String,

        @ColumnInfo(name = "time")
        var time: String?,

        @ColumnInfo(name = "rating")
        var rating: String?,

        @ColumnInfo(name = "year")
        var year: String?,

        @ColumnInfo(name = "poster")
        var poster: String,

        @ColumnInfo(name = "summary")
        var summary: String,

        @ColumnInfo(name = "cast")
        var cast: List<Cast>,

        @ColumnInfo(name = "chapter")
        var chapter: List<Chapter>?
)
