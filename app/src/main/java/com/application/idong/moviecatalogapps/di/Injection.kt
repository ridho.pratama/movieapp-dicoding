package com.application.idong.moviecatalogapps.di

import android.content.Context
import com.application.idong.moviecatalogapps.api.ApiClientAdapter
import com.application.idong.moviecatalogapps.data.MovieRepository
import com.application.idong.moviecatalogapps.data.local.LocalDataSource
import com.application.idong.moviecatalogapps.data.local.database.MovieDatabase
import com.application.idong.moviecatalogapps.data.remote.RemoteDataSource

/**
 * Created by ridhopratama on 20,December,2020
 */

object Injection {
    fun provideRepository(context: Context): MovieRepository {

        val database = MovieDatabase.getInstance(context)
        val remoteDataSource = RemoteDataSource.getInstance(ApiClientAdapter())
        val localDataSource = LocalDataSource.getInstance(database.movieDao())

        return MovieRepository.getInstance(remoteDataSource, localDataSource)
    }
}