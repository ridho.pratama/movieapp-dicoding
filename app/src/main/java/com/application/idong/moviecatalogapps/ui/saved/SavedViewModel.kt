package com.application.idong.moviecatalogapps.ui.saved

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.application.idong.moviecatalogapps.data.MovieRepository
import com.application.idong.moviecatalogapps.data.local.entity.Movies
import kotlinx.coroutines.launch

/**
 * Created by ridhopratama on 10,December,2020
 */

class SavedViewModel(private val movieRepository: MovieRepository): ViewModel() {

    fun getSavedMovies(category: String) = movieRepository.getFavoriteMovies(category)

    fun getFavoriteMoviesById(moviesId: Int) = movieRepository.getFavoriteMoviesById(moviesId)

    fun upsert(movies: Movies) = viewModelScope.launch {
        movieRepository.upsert(movies)
    }

    fun delete(movies: Movies) = viewModelScope.launch {
        movieRepository.delete(movies)
    }
}