package com.application.idong.moviecatalogapps.utils

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by ridhopratama on 25,October,2020
 */

@GlideModule
class MovieGlideModuleApp : AppGlideModule()