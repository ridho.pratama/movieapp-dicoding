package com.application.idong.moviecatalogapps.data.remote.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * Created by ridhopratama on 07,December,2020
 */
@Parcelize
data class MoviesRemote(
        val id: Int,
        val title: String,
        val category: String,
        val genre: String,
        val time: String?,
        val rating: String?,
        val year: String?,
        val poster: String,
        val summary: String,
        val cast: List<CastRemote>,
        val chapter: List<ChapterRemote>?
) : Parcelable
