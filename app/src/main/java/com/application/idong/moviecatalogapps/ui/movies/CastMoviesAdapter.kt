package com.application.idong.moviecatalogapps.ui.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.application.idong.moviecatalogapps.api.UriService
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.databinding.ItemsCastsBinding
import com.application.idong.moviecatalogapps.data.remote.entity.CastRemote
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

/**
 * Created by ridhopratama on 09,December,2020
 */
class CastMoviesAdapter : RecyclerView.Adapter<CastMoviesAdapter.ViewHolder>() {
    private val casts = mutableListOf<CastRemote>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemsCastsBinding = ItemsCastsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemsCastsBinding)
    }

    override fun getItemCount(): Int = casts.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = casts[position]
        holder.bind(data)
    }

    fun updateData(newCasts: MutableList<CastRemote>) {
        casts.clear()
        casts.addAll(newCasts)
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemsCastsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(cast: CastRemote) {
            with(binding) {
                Glide.with(itemView.context)
                    .load(UriService.POSTER_URL + cast.foto)
                    .apply(RequestOptions.placeholderOf(R.drawable.img_loading)
                        .error(R.drawable.img_error))
                    .into(ivFoto)
                tvName.text = cast.name
                tvActionAs.text = cast.actionAs
            }
        }
    }
}