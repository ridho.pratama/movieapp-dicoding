package com.application.idong.moviecatalogapps.ui.home

/**
 * Created by ridhopratama on 07,December,2020
 */
interface CategoryListener {

    fun onSelectCategory(position: Int)
}