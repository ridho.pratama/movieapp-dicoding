package com.application.idong.moviecatalogapps.models

/**
 * Created by ridhopratama on 06,December,2020
 */

data class BottombarModel(
    val itemId: Int,
    val itemIconId: Int,
    val itemTitle: String,
    val itemBackgroundColor: String,
    val itemTintColor: String
) {
    private var isOpen: Boolean = false

    fun setOpen(open: Boolean) {
        isOpen = open
    }

    fun getOpen(): Boolean {
        return isOpen
    }
}