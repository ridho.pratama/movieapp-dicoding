package com.application.idong.moviecatalogapps.data.remote.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * Created by ridhopratama on 07,December,2020
 */
@Parcelize
data class ChapterRemote(
    val id: Int,
    val chapterName: String,
    val chapterTotal: Int,
    val chapterTime: String,
    val chapterYear: String,
    val isCurrent: Boolean
) : Parcelable