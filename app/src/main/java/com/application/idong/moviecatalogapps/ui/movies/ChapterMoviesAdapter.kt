package com.application.idong.moviecatalogapps.ui.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.databinding.ItemsChapterBinding
import com.application.idong.moviecatalogapps.data.remote.entity.ChapterRemote

/**
 * Created by ridhopratama on 09,December,2020
 */
class ChapterMoviesAdapter(private val listener: ChapterListener): RecyclerView.Adapter<ChapterMoviesAdapter.ViewHolder>() {
    private val chapters = mutableListOf<ChapterRemote>()
    private var selectedItem = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemsChapterBinding = ItemsChapterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemsChapterBinding)
    }

    override fun getItemCount(): Int = chapters.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = chapters[position]
        holder.bind(data, position)
    }

    fun updateData(newChapters: MutableList<ChapterRemote>) {
        chapters.clear()
        chapters.addAll(newChapters)
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemsChapterBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(chapter: ChapterRemote, position: Int) {

            with(binding) {
                llChapter.setBackgroundResource(R.drawable.shape_rounded_outline_primary)
                tvChapterName.setTextColor(ContextCompat.getColor(itemView.context, R.color.colorPrimary))
                tvChapterTotal.setTextColor(ContextCompat.getColor(itemView.context, R.color.colorPrimary))
                tvChapterName.text = chapter.chapterName
                tvChapterTotal.text = "${chapter.chapterTotal} Episode"
            }

            if (selectedItem == position) {
                with(binding) {
                    llChapter.setBackgroundResource(R.drawable.shape_roundcorner_primary)
                    tvChapterName.setTextColor(ContextCompat.getColor(itemView.context, R.color.colorWhite))
                    tvChapterTotal.setTextColor(ContextCompat.getColor(itemView.context, R.color.colorWhite))
                }
            }

            itemView.setOnClickListener {
                val previousItem = selectedItem
                selectedItem = position
                notifyItemChanged(previousItem)
                notifyItemChanged(position)
                listener.onSelectChapter(chapter)
            }
        }
    }
}