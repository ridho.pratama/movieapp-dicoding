package com.application.idong.moviecatalogapps.ui.home

import android.annotation.SuppressLint
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.databinding.ItemsCategoryHomeBinding
import com.application.idong.moviecatalogapps.models.Category
import com.application.idong.moviecatalogapps.utils.ColorUtil

/**
 * Created by ridhopratama on 06,December,2020
 */
class CategoryHomeAdapter(private val listener: CategoryListener): RecyclerView.Adapter<CategoryHomeAdapter.ViewHolder>() {

    private val categories = mutableListOf<Category>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemsCategoryHomeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = categories.size

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val category = categories[position]
        holder.bind(category, position)
    }

    fun updateData(newCategories: MutableList<Category>) {
        categories.clear()
        categories.addAll(newCategories)
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemsCategoryHomeBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(category: Category, position: Int) {
            with(binding) {
                val color = ContextCompat.getColor(itemView.context, R.color.colorPrimarySoft)
                llCategoryProductHome.setBackgroundColor(color)
                ivCategoryProductHome.setImageResource(category.icon)
                ivCategoryProductHome.setColorFilter(ColorUtil.darkenColor(color, 0.6f), PorterDuff.Mode.SRC_ATOP)
                tvCategoryProductHome.text = category.title

                itemView.setOnClickListener {
                    listener.onSelectCategory(position)
                }
            }
        }
    }
}