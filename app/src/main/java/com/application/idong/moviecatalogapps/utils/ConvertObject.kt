package com.application.idong.moviecatalogapps.utils

import com.application.idong.moviecatalogapps.data.local.entity.Cast
import com.application.idong.moviecatalogapps.data.local.entity.Chapter
import com.application.idong.moviecatalogapps.data.remote.entity.CastRemote
import com.application.idong.moviecatalogapps.data.remote.entity.ChapterRemote

/**
 * Created by ridhopratama on 02,January,2021
 */
object ConvertObject {

    fun collectionCastRemoteToCast(castRemote: List<CastRemote>): List<Cast> {

        return castRemote.map {
            Cast(
                    it.id,
                    it.name,
                    it.actionAs,
                    it.foto
            )
        }
    }
    fun collectionChapterRemoteToChapter(chapter: List<ChapterRemote>?): List<Chapter>? {
        if (chapter.isNullOrEmpty()) {
            return null
        }

        return chapter.map {
            Chapter(
                    it.id,
                    it.chapterName,
                    it.chapterTotal,
                    it.chapterTime,
                    it.chapterYear,
                    it.isCurrent
            )
        }
    }

    fun collectionCastToRemoteCast(cast: List<Cast>): List<CastRemote> {

        return cast.map {
            CastRemote(
                    it.id,
                    it.name,
                    it.actionAs,
                    it.foto
            )
        }
    }

    fun collectionChapterToChapterRemote(chapter: List<Chapter>?): List<ChapterRemote>? {
        if (chapter.isNullOrEmpty()) {
            return null
        }

        return chapter.map {
            ChapterRemote(
                    it.id,
                    it.chapterName,
                    it.chapterTotal,
                    it.chapterTime,
                    it.chapterYear,
                    it.isCurrent
            )
        }
    }

    fun castToCastRemote(cast: Cast): CastRemote {
        return CastRemote(
                cast.id,
                cast.name,
                cast.actionAs,
                cast.foto
        )
    }

    fun chapterToChapterRemote(chapter: Chapter): ChapterRemote {
        return ChapterRemote(
                chapter.id,
                chapter.chapterName,
                chapter.chapterTotal,
                chapter.chapterTime,
                chapter.chapterYear,
                chapter.isCurrent
        )
    }
}