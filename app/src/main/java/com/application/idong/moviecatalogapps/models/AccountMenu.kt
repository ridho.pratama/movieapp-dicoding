package com.application.idong.moviecatalogapps.models

/**
 * Created by ridhopratama on 06,December,2020
 */

data class AccountMenu(var idMenu: Int, var labelMenu: String, var iconMenu: Int)