package com.application.idong.moviecatalogapps.ui.movies

import com.application.idong.moviecatalogapps.data.remote.entity.ChapterRemote

/**
 * Created by ridhopratama on 09,December,2020
 */
interface ChapterListener {

    fun onSelectChapter(chapter: ChapterRemote)
}