package com.application.idong.moviecatalogapps.data.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.application.idong.moviecatalogapps.data.local.entity.Movies

/**
 * Created by ridhopratama on 29,December,2020
 */

@Database(
    entities = [Movies::class],
    version = 1,
    exportSchema = false)
@TypeConverters(RoomConverter::class)
abstract class MovieDatabase: RoomDatabase() {

    abstract fun movieDao(): MovieDao

    companion object {

        @Volatile
        private var INSTANCE: MovieDatabase? = null

        fun getInstance(context: Context): MovieDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: Room.databaseBuilder(context.applicationContext,
                    MovieDatabase::class.java,
                    "movies.db").build()
            }
    }

}