package com.application.idong.moviecatalogapps.ui.movies

import com.application.idong.moviecatalogapps.data.local.entity.Movies
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote

/**
 * Created by ridhopratama on 09,December,2020
 */
interface MovieListener {
    fun selectMovie(movie: MoviesRemote)
    fun selectMovieOnHome(type: String, movie: MoviesRemote)
    fun selectFavoriteMovie(movie: Movies)
}