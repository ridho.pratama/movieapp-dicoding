package com.application.idong.moviecatalogapps.api

import com.application.idong.moviecatalogapps.data.remote.response.MovieListResponse
import com.application.idong.moviecatalogapps.data.remote.response.MovieResponse
import retrofit2.Response
import retrofit2.http.*

/**
 * Created by ridhopratama on 20,December,2020
 */

interface MoviesService {

    @GET(UriService.POPULAR)
    suspend fun popular(): Response<MovieListResponse>

    @GET(UriService.NEWEST)
    suspend fun newest(): Response<MovieListResponse>

    @GET(UriService.MOVIES)
    suspend fun movies(): Response<MovieListResponse>

    @GET(UriService.TVSHOW)
    suspend fun tvShow(): Response<MovieListResponse>

    @GET(UriService.SAVED)
    suspend fun saved(): Response<MovieListResponse>

    @GET(UriService.POPULAR_DETAIL)
    suspend fun popularDetail(@Path("id") id: Int): Response<MovieResponse>

    @GET(UriService.NEWEST_DETAIL)
    suspend fun newestDetail(@Path("id") id: Int): Response<MovieResponse>

    @GET(UriService.MOVIES_DETAIL)
    suspend fun moviesDetail(@Path("id") id: Int): Response<MovieResponse>

    @GET(UriService.TVSHOW_DETAIL)
    suspend fun tvshowDetail(@Path("id") id: Int): Response<MovieResponse>

    @GET(UriService.SAVED_DETAIL)
    suspend fun savedDetail(@Path("id") id: Int): Response<MovieResponse>
}