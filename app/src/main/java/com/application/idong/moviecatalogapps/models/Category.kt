package com.application.idong.moviecatalogapps.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * Created by ridhopratama on 06,December,2020
 */

@Parcelize
data class Category(
    var id: Int,
    var icon: Int,
    var title: String
) : Parcelable