package com.application.idong.moviecatalogapps.utils

/**
 * Created by ridhopratama on 27,December,2020
 */
object Constant {
    const val DELAY_SPLASHSCREEN = 2000L
    const val MESSAGE_SERVERERROR = "Internal Server Error"
}