package com.application.idong.moviecatalogapps.utils

import android.content.Context
import androidx.core.content.ContextCompat
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.models.AccountMenu
import com.application.idong.moviecatalogapps.models.BottombarModel
import com.application.idong.moviecatalogapps.models.Category

/**
 * Created by ridhopratama on 06,December,2020
 */

object Menu {

    fun mainMenu(context: Context): ArrayList<BottombarModel> {
        return arrayListOf(
                BottombarModel(
                        0,
                        R.drawable.ic_home_line,
                        "Home",
                        "#" + Integer.toHexString(
                                ContextCompat.getColor(
                                        context,
                                        R.color.colorPrimarySoft
                                )
                        ),
                        "#" + Integer.toHexString(
                                ContextCompat.getColor(
                                        context,
                                        R.color.colorPrimary
                                )
                        )
                ),
                BottombarModel(
                        1,
                        R.drawable.ic_bookmark_line,
                        "Saved",
                        "#" + Integer.toHexString(
                                ContextCompat.getColor(
                                        context,
                                        R.color.colorPrimarySoft
                                )
                        ),
                        "#" + Integer.toHexString(
                                ContextCompat.getColor(
                                        context,
                                        R.color.colorPrimary
                                )
                        )
                ),
                BottombarModel(
                        2,
                        R.drawable.ic_account_line,
                        "Account",
                        "#" + Integer.toHexString(
                                ContextCompat.getColor(
                                        context,
                                        R.color.colorPrimarySoft
                                )
                        ),
                        "#" + Integer.toHexString(
                                ContextCompat.getColor(
                                        context,
                                        R.color.colorPrimary
                                )
                        )
                )
        )
    }

    fun accountMenu(): MutableList<AccountMenu> {
        return mutableListOf(
                AccountMenu(0, "Profile", R.drawable.ic_account_line),
                AccountMenu(1, "Notification", R.drawable.ic_notification_line),
                AccountMenu(2, "FAQ", R.drawable.ic_faq_line)
        )
    }

    fun categorySample(): MutableList<Category> {
        return mutableListOf(
                Category(0, R.drawable.ic_movie, "Movies"),
                Category(1, R.drawable.ic_tvshow, "TV Show")
        )
    }
}