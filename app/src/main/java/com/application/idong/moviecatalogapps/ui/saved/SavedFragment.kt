package com.application.idong.moviecatalogapps.ui.saved

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.data.local.entity.Movies
import com.application.idong.moviecatalogapps.ui.movies.MovieListener
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote
import com.application.idong.moviecatalogapps.databinding.FragmentSavedBinding
import com.application.idong.moviecatalogapps.factory.ViewModelFactory
import com.google.android.material.snackbar.Snackbar

/**
 * Created by ridhopratama on 07,December,2020
 */

class SavedFragment : Fragment(), MovieListener {

    private lateinit var binding: FragmentSavedBinding
    private lateinit var viewModel: SavedViewModel
    private lateinit var adapterMovies: SavedMoviesAdapter
    private var position = 0

    companion object {

        const val ARG_POSITION = "position"

        fun newInstance(position: Int) : SavedFragment {
            val fragment = SavedFragment()
            fragment.arguments = bundleOf(ARG_POSITION to position)
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSavedBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments != null) {
            position = arguments?.getInt(ARG_POSITION, 0) as Int
        }
        val factory = ViewModelFactory.getInstance(requireActivity())
        viewModel = ViewModelProvider(this, factory).get(SavedViewModel::class.java)
        adapterMovies = SavedMoviesAdapter(this)
        itemTouchHelper.attachToRecyclerView(binding.rvSavedMovie)
    }

    override fun onResume() {
        super.onResume()
        loadSavedMovie()
    }

    private fun loadSavedMovie() {
        val category = when(position) {
            0 -> getString(R.string.title_movies)
            else -> getString(R.string.title_tvshow)
        }
        viewModel.getSavedMovies(category).observe(viewLifecycleOwner, { movies ->
            binding.smFragmentSaved.visibility = View.GONE
            binding.llEmpty.visibility = View.GONE
            binding.rvSavedMovie.visibility  = View.VISIBLE
            adapterMovies.submitList(movies)
            if (movies.isNullOrEmpty()) {
                binding.rvSavedMovie.visibility = View.GONE
                binding.llEmpty.visibility = View.VISIBLE
            }
        })

        with(binding.rvSavedMovie) {
            setHasFixedSize(true)
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(requireContext())
            adapter = adapterMovies
        }
    }

    private val itemTouchHelper = ItemTouchHelper(object : ItemTouchHelper.Callback() {
        override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int =
                makeMovementFlags(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT)

        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean = true

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            if (view != null) {
                val swipedPosition = viewHolder.adapterPosition
                val movie = adapterMovies.getSwipedData(swipedPosition)
                movie?.let {
                    viewModel.delete(movie)
                }
                val snackbar = Snackbar.make(view as View, getString(R.string.title_delete_favorite_undo), Snackbar.LENGTH_LONG)
                snackbar.setAction("OK") {
                    movie?.let {
                        viewModel.upsert(movie)
                    }
                }
                snackbar.show()
            }
        }

    })

    override fun selectMovie(movies: MoviesRemote) {

    }

    override fun selectMovieOnHome(type: String, movie: MoviesRemote) {

    }

    override fun selectFavoriteMovie(movie: Movies) {
        val intent = Intent(requireContext(), DetailMovieSavedActivity::class.java)
        intent.putExtra(DetailMovieSavedActivity.EXTRA_MOVIEID, movie.moviesId)
        startActivity(intent)
    }

}