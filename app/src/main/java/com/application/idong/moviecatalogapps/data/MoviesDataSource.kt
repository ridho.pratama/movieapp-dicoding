package com.application.idong.moviecatalogapps.data

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.application.idong.moviecatalogapps.data.local.entity.Movies
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote

/**
 * Created by ridhopratama on 20,December,2020
 */
interface MoviesDataSource {

    suspend fun getRemotePopular(): List<MoviesRemote>

    suspend fun getRemoteNewest(): List<MoviesRemote>

    suspend fun getRemoteMovies(): List<MoviesRemote>

    suspend fun getRemoteTvshow(): List<MoviesRemote>

    suspend fun getRemoteSaved(): List<MoviesRemote>

    suspend fun getRemotePopularDetail(id: Int): MoviesRemote

    suspend fun getRemoteNewestDetail(id: Int): MoviesRemote

    suspend fun getRemoteMoviesDetail(id: Int): MoviesRemote

    suspend fun getRemoteTvshowDetail(id: Int): MoviesRemote

    suspend fun getRemoteSavedDetail(id: Int): MoviesRemote

    fun getFavoriteMovies(category: String) : LiveData<PagedList<Movies>>

    fun getFavoriteMoviesById(id: Int): LiveData<Movies>

    suspend fun upsert(movies: Movies)

    suspend fun delete(movies: Movies)
}