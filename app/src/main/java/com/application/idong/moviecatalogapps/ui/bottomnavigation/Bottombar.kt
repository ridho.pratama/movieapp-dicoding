package com.application.idong.moviecatalogapps.ui.bottomnavigation

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.View
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.models.BottombarModel
import kotlin.collections.ArrayList

/**
 * Created by ridhopratama on 06,December,2020
 */

class Bottombar(context: Context, view: View, itemSelectorInterface: BottombarAdapter.ItemSelectorInterface) {

    private var context: Context? = null
    private val itemLimit = 5
    private var customBottombarParent: CardView? = null
    private var customRecyclerView: RecyclerView? = null
    private var customDivider: View? = null
    private var items: ArrayList<BottombarModel> = arrayListOf()
    @SuppressLint("ResourceType")
    private var defaultBackground = context.getString(R.color.colorWhite)
    @SuppressLint("ResourceType")
    private var defaultTint = context.getString(R.color.colorBlack)
    private var bottombarAdapter: BottombarAdapter? = null
    private var itemSelectorInterface: BottombarAdapter.ItemSelectorInterface

    init {
        setView(view)
        this.context = context
        this.itemSelectorInterface = itemSelectorInterface
    }

    private fun setView(view: View) {
        customBottombarParent = view.findViewById(R.id.custom_bottom_bar_parent)
        customRecyclerView = view.findViewById(R.id.rvBottomMenu)
        customDivider = view.findViewById(R.id.custom_divider)
    }

    private fun addItem(item: BottombarModel) {
        if (items.size <= itemLimit - 1) {
            items.add(item)
        }
    }

    fun addAllItem(itemData: ArrayList<BottombarModel>) {
        items.clear()
        items.addAll(itemData)
    }

    fun changeBackground(color: String) {
        customBottombarParent?.setCardBackgroundColor(Color.parseColor(color))
    }

    fun changeDividerColor(color: String) {
        customDivider?.setBackgroundColor(Color.parseColor(color))
    }

    fun hideDivider() {
        customDivider?.visibility = View.GONE
    }

    fun getDefaultBackground(): String {
        return defaultBackground
    }

    fun setDefaultBackground(defaultBackground: String) {
        this.defaultBackground = defaultBackground
    }

    fun getDefaultTint(): String {
        return defaultTint
    }

    fun setDefaultTint(defaultTint: String) {
        this.defaultTint = defaultTint
    }

    private fun setAdapter(defaultOpenIndex: Int) {
        bottombarAdapter = BottombarAdapter(defaultOpenIndex, items, itemSelectorInterface)
        bottombarAdapter?.setDefaultBackground(getDefaultBackground())
        bottombarAdapter?.setDefaultTint(getDefaultTint())
        customRecyclerView?.layoutManager = GridLayoutManager(context, items.size)
        customRecyclerView?.adapter = bottombarAdapter
    }

    fun apply(defaultOpenIndex: Int) {
        setAdapter(defaultOpenIndex)
    }
}