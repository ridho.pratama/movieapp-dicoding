package com.application.idong.moviecatalogapps.utils

import com.application.idong.moviecatalogapps.data.remote.entity.CastRemote
import com.application.idong.moviecatalogapps.data.remote.entity.ChapterRemote
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote

/**
 * Created by ridhopratama on 07,December,2020
 */

object DataMoviesRemote {

    fun popular() : MutableList<MoviesRemote> {
        return mutableListOf(
                MoviesRemote(
                        1006, "A Star Is Born", "Movies", "Drama, Romance, Music", "2h 16m", "4.5", "2018", "movie_poster_a_start_is_born.jpg", "Seasoned musician Jackson Maine discovers — and falls in love with — struggling artist Ally. She has just about given up on her dream to make it big as a singer — until Jack coaxes her into the spotlight. But even as Ally's career takes off, the personal side of their relationship is breaking down, as Jack fights an ongoing battle with his own internal demons.", listOf(
                        CastRemote(
                                1,
                                "Jackson Maine",
                                "Bradley Cooper",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Ally Campana",
                                "Lady Gaga",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Sam Elliott",
                                "Bobby Maine",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Andrew Dice Clay",
                                "Lorenzo Campana",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        2008, "Game of Thrones", "TV Show", "Sci-Fi & Fantasy, Drama, Action & Adventure, Mystery", "1h", "4.9", "2019", "tvshow_poster_god.jpg", "Seven noble families fight for control of the mythical land of Westeros. Friction between the houses leads to full-scale war. All while a very ancient evil awakens in the farthest north. Amidst the war, a neglected military order of misfits, the Night's Watch, is all that stands between the realms of men and icy horrors beyond.", listOf(
                        CastRemote(
                                1,
                                "Emilia Clarke",
                                "Daenerys Targaryen",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                2,
                                "Kit Harington",
                                "Jon Snow",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Sophie Turner",
                                "Sansa Stark",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Peter Dinklage",
                                "Tyrion Lannister",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        10,
                                        "1h",
                                        "2011",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        10,
                                        "1h",
                                        "2012",
                                        false
                                ),
                                ChapterRemote(
                                        3,
                                        "Season 3",
                                        10,
                                        "1h",
                                        "2013",
                                        false
                                ),
                                ChapterRemote(
                                        4,
                                        "Season 4",
                                        10,
                                        "1h",
                                        "2014",
                                        false
                                ),
                                ChapterRemote(
                                        5,
                                        "Season 5",
                                        10,
                                        "1h",
                                        "2015",
                                        false
                                ),
                                ChapterRemote(
                                        6,
                                        "Season 6",
                                        10,
                                        "1h",
                                        "2016",
                                        false
                                ),
                                ChapterRemote(
                                        7,
                                        "Season 7",
                                        7,
                                        "1h",
                                        "2017",
                                        false
                                ),
                                ChapterRemote(
                                        8,
                                        "Season 8",
                                        6,
                                        "1h",
                                        "2019",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        1008, "Aquaman", "Movies", "Action, Adventure, Fantasy", "2h 24m", "4.1", "2018", "movie_poster_aquaman.jpg", "Once home to the most advanced civilization on Earth, Atlantis is now an underwater kingdom ruled by the power-hungry King Orm. With a vast army at his disposal, Orm plans to conquer the remaining oceanic people and then the surface world. Standing in his way is Arthur Curry, Orm's half-human, half-Atlantean brother and true heir to the throne.", listOf(
                        CastRemote(
                                1,
                                "Jason Momoa",
                                "Aquaman",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Amber Heard",
                                "Mera",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Willem Dafoe",
                                "Nuidis Vulko",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Patrick Wilson",
                                "Ocean Master",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        1009, "Bohemian Rhapsody", "Movies", "Drama, Music", "2h 15m", "4.8", "2018", "movie_poster_bohemian.jpg", "Singer Freddie Mercury, guitarist Brian May, drummer Roger Taylor and bass guitarist John Deacon take the music world by storm when they form the rock 'n' roll band Queen in 1970. Hit songs become instant classics. When Mercury's increasingly wild lifestyle starts to spiral out of control, Queen soon faces its greatest challenge yet – finding a way to keep the band together amid the success and excess.", listOf(
                        CastRemote(
                                1,
                                "Rami Malek",
                                "Freddie Mercury",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Lucy Boynton",
                                "Mary Austin",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Gwilym Lee",
                                "Brian May",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Ben Hardy",
                                "Roger Taylor",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        1010, "Cold Pursuit", "Movies", "Action, Crime, Thriller", "1h 59m", "4.0", "2019", "movie_poster_cold_persuit.jpg", "The quiet family life of Nels Coxman, a snowplow driver, is upended after his son's murder. Nels begins a vengeful hunt for Viking, the drug lord he holds responsible for the killing, eliminating Viking's associates one by one. As Nels draws closer to Viking, his actions bring even more unexpected and violent consequences, as he proves that revenge is all in the execution.", listOf(
                        CastRemote(
                                1,
                                "Liam Neeson",
                                "Nels Coxman",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Laura Dern",
                                "Grace Coxman",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Emmy Rossum",
                                "Kim Dash",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Michael Eklund",
                                "Speedo",
                                "img_actor.jpg"
                        )
                ),
                        null
                )
        )
    }

    fun newest() : MutableList<MoviesRemote> {
        return mutableListOf(
                MoviesRemote(
                        2006, "Doom Patrol", "TV Show", "Sci-Fi & Fantasy, Action & Adventure", "49m", "4.6", "2020", "tvshow_poster_doom_patrol.jpg", "The Doom Patrol’s members each suffered horrible accidents that gave them superhuman abilities — but also left them scarred and disfigured. Traumatized and downtrodden, the team found purpose through The Chief, who brought them together to investigate the weirdest phenomena in existence — and to protect Earth from what they find.", listOf(
                        CastRemote(
                                1,
                                "Diane Guerrero",
                                "Crazy Jane",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                2,
                                "April Bowlby",
                                "Elasti-Woman",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Joivan Wade",
                                "Cyborg",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Matt Bomer",
                                "Negative Man",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        15,
                                        "49m",
                                        "2019",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        9,
                                        "49m",
                                        "2020",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        2007, "Hanna", "TV Show", "Action & Adventure, Drama", "50m", "4.4", "2020", "tvshow_poster_hanna.jpg", "This thriller and coming-of-age drama follows the journey of an extraordinary young girl as she evades the relentless pursuit of an off-book CIA agent and tries to unearth the truth behind who she is. Based on the 2011 Joe Wright film.", listOf(
                        CastRemote(
                                1,
                                "Esme Creed-Miles",
                                "Hanna",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                2,
                                "Mireille Enos",
                                "Marissa",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Noah Taylor",
                                "Dr. Roland Kunek",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Joel Kinnaman",
                                "Erik Heller",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        8,
                                        "50m",
                                        "2019",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        8,
                                        "50m",
                                        "2020",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        1007, "Alita: Battle Angel", "Movies", "Action, Science Fiction, Adventure", "2h 2m", "4.2", "2019", "movie_poster_alita.jpg", "When Alita awakens with no memory of who she is in a future world she does not recognize, she is taken in by Ido, a compassionate doctor who realizes that somewhere in this abandoned cyborg shell is the heart and soul of a young woman with an extraordinary past.", listOf(
                        CastRemote(
                                1,
                                "Rosa Salazar",
                                "Alita",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                2,
                                "Christoph Waltz",
                                "Dr. Dyson Ido",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Jennifer Connelly",
                                "Chiren",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Mahershala Ali",
                                "Vector",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        2009, "Gotham", "TV Show", "Drama, Fantasy, Crime", "43m", "4.5", "2019", "tvshow_poster_gotham.jpg", "Everyone knows the name Commissioner Gordon. He is one of the crime world's greatest foes, a man whose reputation is synonymous with law and order. But what is known of Gordon's story and his rise from rookie detective to Police Commissioner? What did it take to navigate the multiple layers of corruption that secretly ruled Gotham City, the spawning ground of the world's most iconic villains? And what circumstances created them – the larger-than-life personas who would become Catwoman, The Penguin, The Riddler, Two-Face and The Joker?", listOf(
                        CastRemote(
                                1,
                                "Ben McKenzie",
                                "James Gordon",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Donal Logue",
                                "Harvey Bullock",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Erin Richards",
                                "Barbara Kean",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "David Mazouz",
                                "Batman",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        22,
                                        "43m",
                                        "2014",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 1",
                                        22,
                                        "43m",
                                        "2015",
                                        false
                                ),
                                ChapterRemote(
                                        3,
                                        "Season 3",
                                        22,
                                        "43m",
                                        "2016",
                                        false
                                ),
                                ChapterRemote(
                                        4,
                                        "Season 4",
                                        22,
                                        "43m",
                                        "2017",
                                        false
                                ),
                                ChapterRemote(
                                        5,
                                        "Season 5",
                                        12,
                                        "43m",
                                        "2019",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        2010, "Marvel's Iron Fist", "TV Show", "Action & Adventure, Drama, Sci-Fi & Fantasy", "55m", "4.2", "2018", "tvshow_poster_iron_fist.jpg", "Danny Rand resurfaces 15 years after being presumed dead. Now, with the power of the Iron Fist, he seeks to reclaim his past and fulfill his destiny.", listOf(
                        CastRemote(
                                1,
                                "Finn Jones",
                                "Danny Rand",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Jessica Henwick",
                                "Colleen Wing",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Jessica Stroup",
                                "Joy Meachum",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Tom Pelphrey",
                                "Ward Meachum",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        13,
                                        "55m",
                                        "2017",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        10,
                                        "55m",
                                        "2018",
                                        true
                                )
                        )
                )
        )
    }

    fun movies() : MutableList<MoviesRemote> {
        return mutableListOf(
                MoviesRemote(
                        1001, "Creed II", "Movies", "Drama", "2h 10m", "4.3", "2018", "movie_poster_creed.jpg", "Between personal obligations and training for his next big fight against an opponent with ties to his family's past, Adonis Creed is up against the challenge of his life.", listOf(
                        CastRemote(
                                1,
                                "Michael B. Jordan",
                                "Adonis Creed",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Sylvester Stallone",
                                "Robert \"Rocky\" Balboa Sr.",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Dolph Lundgren",
                                "Ivan Drago",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Tessa Thompson",
                                "Bianca Taylor",
                                "img_actor_women.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        1002, "Fantastic Beasts: The Crimes of Grindelwald", "Movies", "Adventure, Fantasy, Drama", "2h 14m", "4.4", "2018", "movie_poster_crimes.jpg", "Gellert Grindelwald has escaped imprisonment and has begun gathering followers to his cause—elevating wizards above all non-magical beings. The only one capable of putting a stop to him is the wizard he once called his closest friend, Albus Dumbledore. However, Dumbledore will need to seek help from the wizard who had thwarted Grindelwald once before, his former student Newt Scamander, who agrees to help, unaware of the dangers that lie ahead. Lines are drawn as love and loyalty are tested, even among the truest friends and family, in an increasingly divided wizarding world.", listOf(
                        CastRemote(
                                1,
                                "Eddie Redmayne",
                                "Newt Scamander",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Katherine Waterston",
                                "Porpentina \"Tina\" Goldstein",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Alison Sudol",
                                "Queenie Goldstein",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Johnny Depp",
                                "Gellert Grindelwald",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        1003, "Glass", "Movies", "Thriller, Drama, Science Fiction", "2h 9m", "4.6", "2019", "movie_poster_glass.jpg", "In a series of escalating encounters, former security guard David Dunn uses his supernatural abilities to track Kevin Wendell Crumb, a disturbed man who has twenty-four personalities. Meanwhile, the shadowy presence of Elijah Price emerges as an orchestrator who holds secrets critical to both men.", listOf(
                        CastRemote(
                                1,
                                "James McAvoy",
                                "Hedwig etc",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Bruce Willis",
                                "The Overseer",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Anya Taylor-Joy",
                                "Casey Cooke",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Samuel L. Jackson",
                                "Mr. Glass",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        1004, "How to Train Your Dragon: The Hidden World", "Movies", "Animation, Family, Adventure", "1h 44m", "4.7", "2019", "movie_poster_how_to_train.jpg", "As Hiccup fulfills his dream of creating a peaceful dragon utopia, Toothless’ discovery of an untamed, elusive mate draws the Night Fury away. When danger mounts at home and Hiccup’s reign as village chief is tested, both dragon and rider must make impossible decisions to save their kind.", listOf(
                        CastRemote(
                                1,
                                "Jay Baruchel",
                                "Hiccup (voice)",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "America Ferrera",
                                "Astrid (voice)",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "F. Murray Abraham",
                                "Grimmel (voice)",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Cate Blanchett",
                                "Valka (voice)",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        1005, "Avengers: Infinity War", "Movies", "Adventure, Action, Science Fiction", "2h 29m", "4.8", "2018", "movie_poster_infinity_war.jpg", "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.", listOf(
                        CastRemote(
                                1,
                                "Robert Downey Jr.",
                                "Tony Stark",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Chris Hemsworth",
                                "Thor Odinson",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Scarlett Johansson",
                                "Black Widow",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Chris Evans",
                                "Captain America",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        1006, "A Star Is Born", "Movies", "Drama, Romance, Music", "2h 16m", "4.5", "2018", "movie_poster_a_start_is_born.jpg", "Seasoned musician Jackson Maine discovers — and falls in love with — struggling artist Ally. She has just about given up on her dream to make it big as a singer — until Jack coaxes her into the spotlight. But even as Ally's career takes off, the personal side of their relationship is breaking down, as Jack fights an ongoing battle with his own internal demons.", listOf(
                        CastRemote(
                                1,
                                "Jackson Maine",
                                "Bradley Cooper",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Ally Campana",
                                "Lady Gaga",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Sam Elliott",
                                "Bobby Maine",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Andrew Dice Clay",
                                "Lorenzo Campana",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        1007, "Alita: Battle Angel", "Movies", "Action, Science Fiction, Adventure", "2h 2m", "4.2", "2019", "movie_poster_alita.jpg", "When Alita awakens with no memory of who she is in a future world she does not recognize, she is taken in by Ido, a compassionate doctor who realizes that somewhere in this abandoned cyborg shell is the heart and soul of a young woman with an extraordinary past.", listOf(
                        CastRemote(
                                1,
                                "Rosa Salazar",
                                "Alita",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                2,
                                "Christoph Waltz",
                                "Dr. Dyson Ido",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Jennifer Connelly",
                                "Chiren",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Mahershala Ali",
                                "Vector",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        1008, "Aquaman", "Movies", "Action, Adventure, Fantasy", "2h 24m", "4.1", "2018", "movie_poster_aquaman.jpg", "Once home to the most advanced civilization on Earth, Atlantis is now an underwater kingdom ruled by the power-hungry King Orm. With a vast army at his disposal, Orm plans to conquer the remaining oceanic people and then the surface world. Standing in his way is Arthur Curry, Orm's half-human, half-Atlantean brother and true heir to the throne.", listOf(
                        CastRemote(
                                1,
                                "Jason Momoa",
                                "Aquaman",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Amber Heard",
                                "Mera",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Willem Dafoe",
                                "Nuidis Vulko",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Patrick Wilson",
                                "Ocean Master",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        1009, "Bohemian Rhapsody", "Movies", "Drama, Music", "2h 15m", "4.8", "2018", "movie_poster_bohemian.jpg", "Singer Freddie Mercury, guitarist Brian May, drummer Roger Taylor and bass guitarist John Deacon take the music world by storm when they form the rock 'n' roll band Queen in 1970. Hit songs become instant classics. When Mercury's increasingly wild lifestyle starts to spiral out of control, Queen soon faces its greatest challenge yet – finding a way to keep the band together amid the success and excess.", listOf(
                        CastRemote(
                                1,
                                "Rami Malek",
                                "Freddie Mercury",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Lucy Boynton",
                                "Mary Austin",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Gwilym Lee",
                                "Brian May",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Ben Hardy",
                                "Roger Taylor",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        1010, "Cold Pursuit", "Movies", "Action, Crime, Thriller", "1h 59m", "4.0", "2019", "movie_poster_cold_persuit.jpg", "The quiet family life of Nels Coxman, a snowplow driver, is upended after his son's murder. Nels begins a vengeful hunt for Viking, the drug lord he holds responsible for the killing, eliminating Viking's associates one by one. As Nels draws closer to Viking, his actions bring even more unexpected and violent consequences, as he proves that revenge is all in the execution.", listOf(
                        CastRemote(
                                1,
                                "Liam Neeson",
                                "Nels Coxman",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Laura Dern",
                                "Grace Coxman",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Emmy Rossum",
                                "Kim Dash",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Michael Eklund",
                                "Speedo",
                                "img_actor.jpg"
                        )
                ),
                        null
                )
        )
    }

    fun tvshow() : MutableList<MoviesRemote> {
        return mutableListOf(
                MoviesRemote(
                        2001, "Riverdale", "TV Show", "Drama, Mystery", "45m", "4.9", "2019", "tvshow_poster_riverdale.jpg", "Set in the present, the series offers a bold, subversive take on Archie, Betty, Veronica and their friends, exploring the surreality of small-town life, the darkness and weirdness bubbling beneath Riverdale’s wholesome facade.", listOf(
                        CastRemote(
                                1,
                                "Lili Reinhart",
                                "Elizabeth \"Betty\" Cooper",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                2,
                                "Casey Cott",
                                "Kevin Keller",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Camila Mendes",
                                "Veronica Lodge",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Marisol Nichols",
                                "Hermione Lodge",
                                "img_actor_women.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        13,
                                        "45m",
                                        "2017",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        22,
                                        "45m",
                                        "2017",
                                        false
                                ),
                                ChapterRemote(
                                        3,
                                        "Season 3",
                                        22,
                                        "45m",
                                        "2018",
                                        false
                                ),
                                ChapterRemote(
                                        4,
                                        "Season 4",
                                        22,
                                        "45m",
                                        "2019",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        2002, "Supergirl", "TV Show", "Action, Adventure, Drama, Science Fiction", "42m", "4.4", "2019", "tvshow_poster_supergirl.jpg", "Twenty-four-year-old Kara Zor-El, who was taken in by the Danvers family when she was 13 after being sent away from Krypton, must learn to embrace her powers after previously hiding them. The Danvers teach her to be careful with her powers, until she has to reveal them during an unexpected disaster, setting her on her journey of heroism.", listOf(
                        CastRemote(
                                1,
                                "David Harewood",
                                "Cyborg Superman",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Melissa Benoist",
                                "Supergirl",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Chyler Leigh",
                                "Alex Danvers",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Mehcad Brooks",
                                "Guardian",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        20,
                                        "42m",
                                        "2015",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        22,
                                        "42m",
                                        "2016",
                                        false
                                ),
                                ChapterRemote(
                                        3,
                                        "Season 3",
                                        23,
                                        "42m",
                                        "2017",
                                        false
                                ),
                                ChapterRemote(
                                        4,
                                        "Season 4",
                                        22,
                                        "42m",
                                        "2018",
                                        false
                                ),
                                ChapterRemote(
                                        5,
                                        "Season 5",
                                        19,
                                        "42m",
                                        "2019",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        2003, "Tom Hopper", "TV Show", "Action & Adventure, Sci-Fi & Fantasy, Comedy, Drama", "55m", "4.6", "2020", "tvshow_poster_the_umbrella.jpg", "A dysfunctional family of superheroes comes together to solve the mystery of their father's death, the threat of the apocalypse and more.", listOf(
                        CastRemote(
                                1,
                                "Tom Hopper",
                                "Luther Hargreeves",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "David Castañeda",
                                "Diego Hargreeves",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Emmy Raver-Lampman",
                                "Allison Hargreeves",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Robert Sheehan",
                                "Klaus Hargreeves",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        10,
                                        "55m",
                                        "2019",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        10,
                                        "55m",
                                        "2020",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        2004, "The Walking Dead", "TV Show", "Action & Adventure, Drama, Sci-Fi & Fantasy", "42m", "4.8", "2019", "tvshow_poster_the_walking_dead.jpg", "Sheriff's deputy Rick Grimes awakens from a coma to find a post-apocalyptic world dominated by flesh-eating zombies. He sets out to find his family and encounters many other survivors along the way.", listOf(
                        CastRemote(
                                1,
                                "Norman Reedus",
                                "Daryl Dixon",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Melissa McBride",
                                "Carol Peletier",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Lauren Cohan",
                                "Maggie Greene",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Danai Gurira",
                                "Michonne",
                                "img_actor_women.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        6,
                                        "42m",
                                        "2010",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        13,
                                        "42m",
                                        "2011",
                                        false
                                ),
                                ChapterRemote(
                                        3,
                                        "Season 3",
                                        16,
                                        "42m",
                                        "2012",
                                        false
                                ),
                                ChapterRemote(
                                        4,
                                        "Season 4",
                                        16,
                                        "42m",
                                        "2013",
                                        false
                                ),
                                ChapterRemote(
                                        5,
                                        "Season 5",
                                        16,
                                        "42m",
                                        "2014",
                                        false
                                ),
                                ChapterRemote(
                                        6,
                                        "Season 6",
                                        16,
                                        "42m",
                                        "2015",
                                        false
                                ),
                                ChapterRemote(
                                        7,
                                        "Season 7",
                                        16,
                                        "42m",
                                        "2016",
                                        false
                                ),
                                ChapterRemote(
                                        8,
                                        "Season 8",
                                        16,
                                        "42m",
                                        "2017",
                                        false
                                ),
                                ChapterRemote(
                                        9,
                                        "Season 9",
                                        16,
                                        "42m",
                                        "2018",
                                        false
                                ),
                                ChapterRemote(
                                        10,
                                        "Season 10",
                                        22,
                                        "42m",
                                        "2019",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        2005, "Arrow", "TV Show", "Crime, Drama, Mystery, Action & Adventure", "42m", "4.3", "2019", "tvshow_poster_arrow.jpg", "Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow.", listOf(
                        CastRemote(
                                1,
                                "Stephen Amell",
                                "Green Arrow",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "David Ramsey",
                                "Spartan",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Emily Bett Rickards",
                                "Felicity Smoak",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Katie Cassidy",
                                "Black Siren",
                                "img_actor_women.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        23,
                                        "42m",
                                        "2012",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        23,
                                        "42m",
                                        "2013",
                                        false
                                ),
                                ChapterRemote(
                                        3,
                                        "Season 3",
                                        23,
                                        "42m",
                                        "2014",
                                        false
                                ),
                                ChapterRemote(
                                        4,
                                        "Season 4",
                                        23,
                                        "42m",
                                        "2015",
                                        false
                                ),
                                ChapterRemote(
                                        5,
                                        "Season 5",
                                        23,
                                        "42m",
                                        "2016",
                                        false
                                ),
                                ChapterRemote(
                                        6,
                                        "Season 6",
                                        23,
                                        "42m",
                                        "2017",
                                        false
                                ),
                                ChapterRemote(
                                        7,
                                        "Season 7",
                                        22,
                                        "42m",
                                        "2018",
                                        false
                                ),
                                ChapterRemote(
                                        8,
                                        "Season 8",
                                        10,
                                        "42m",
                                        "2019",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        2006, "Doom Patrol", "TV Show", "Sci-Fi & Fantasy, Action & Adventure", "49m", "4.6", "2020", "tvshow_poster_doom_patrol.jpg", "The Doom Patrol’s members each suffered horrible accidents that gave them superhuman abilities — but also left them scarred and disfigured. Traumatized and downtrodden, the team found purpose through The Chief, who brought them together to investigate the weirdest phenomena in existence — and to protect Earth from what they find.", listOf(
                        CastRemote(
                                1,
                                "Diane Guerrero",
                                "Crazy Jane",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                2,
                                "April Bowlby",
                                "Elasti-Woman",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Joivan Wade",
                                "Cyborg",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Matt Bomer",
                                "Negative Man",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        15,
                                        "49m",
                                        "2019",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        9,
                                        "49m",
                                        "2020",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        2007, "Hanna", "TV Show", "Action & Adventure, Drama", "50m", "4.4", "2020", "tvshow_poster_hanna.jpg", "This thriller and coming-of-age drama follows the journey of an extraordinary young girl as she evades the relentless pursuit of an off-book CIA agent and tries to unearth the truth behind who she is. Based on the 2011 Joe Wright film.", listOf(
                        CastRemote(
                                1,
                                "Esme Creed-Miles",
                                "Hanna",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                2,
                                "Mireille Enos",
                                "Marissa",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Noah Taylor",
                                "Dr. Roland Kunek",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Joel Kinnaman",
                                "Erik Heller",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        8,
                                        "50m",
                                        "2019",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        8,
                                        "50m",
                                        "2020",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        2008, "Game of Thrones", "TV Show", "Sci-Fi & Fantasy, Drama, Action & Adventure, Mystery", "1h", "4.9", "2019", "tvshow_poster_god.jpg", "Seven noble families fight for control of the mythical land of Westeros. Friction between the houses leads to full-scale war. All while a very ancient evil awakens in the farthest north. Amidst the war, a neglected military order of misfits, the Night's Watch, is all that stands between the realms of men and icy horrors beyond.", listOf(
                        CastRemote(
                                1,
                                "Emilia Clarke",
                                "Daenerys Targaryen",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                2,
                                "Kit Harington",
                                "Jon Snow",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Sophie Turner",
                                "Sansa Stark",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Peter Dinklage",
                                "Tyrion Lannister",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        10,
                                        "1h",
                                        "2011",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        10,
                                        "1h",
                                        "2012",
                                        false
                                ),
                                ChapterRemote(
                                        3,
                                        "Season 3",
                                        10,
                                        "1h",
                                        "2013",
                                        false
                                ),
                                ChapterRemote(
                                        4,
                                        "Season 4",
                                        10,
                                        "1h",
                                        "2014",
                                        false
                                ),
                                ChapterRemote(
                                        5,
                                        "Season 5",
                                        10,
                                        "1h",
                                        "2015",
                                        false
                                ),
                                ChapterRemote(
                                        6,
                                        "Season 6",
                                        10,
                                        "1h",
                                        "2016",
                                        false
                                ),
                                ChapterRemote(
                                        7,
                                        "Season 7",
                                        7,
                                        "1h",
                                        "2017",
                                        false
                                ),
                                ChapterRemote(
                                        8,
                                        "Season 8",
                                        6,
                                        "1h",
                                        "2019",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        2009, "Gotham", "TV Show", "Drama, Fantasy, Crime", "43m", "4.5", "2019", "tvshow_poster_gotham.jpg", "Everyone knows the name Commissioner Gordon. He is one of the crime world's greatest foes, a man whose reputation is synonymous with law and order. But what is known of Gordon's story and his rise from rookie detective to Police Commissioner? What did it take to navigate the multiple layers of corruption that secretly ruled Gotham City, the spawning ground of the world's most iconic villains? And what circumstances created them – the larger-than-life personas who would become Catwoman, The Penguin, The Riddler, Two-Face and The Joker?", listOf(
                        CastRemote(
                                1,
                                "Ben McKenzie",
                                "James Gordon",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Donal Logue",
                                "Harvey Bullock",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Erin Richards",
                                "Barbara Kean",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "David Mazouz",
                                "Batman",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        22,
                                        "43m",
                                        "2014",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 1",
                                        22,
                                        "43m",
                                        "2015",
                                        false
                                ),
                                ChapterRemote(
                                        3,
                                        "Season 3",
                                        22,
                                        "43m",
                                        "2016",
                                        false
                                ),
                                ChapterRemote(
                                        4,
                                        "Season 4",
                                        22,
                                        "43m",
                                        "2017",
                                        false
                                ),
                                ChapterRemote(
                                        5,
                                        "Season 5",
                                        12,
                                        "43m",
                                        "2019",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        2010, "Marvel's Iron Fist", "TV Show", "Action & Adventure, Drama, Sci-Fi & Fantasy", "55m", "4.2", "2018", "tvshow_poster_iron_fist.jpg", "Danny Rand resurfaces 15 years after being presumed dead. Now, with the power of the Iron Fist, he seeks to reclaim his past and fulfill his destiny.", listOf(
                        CastRemote(
                                1,
                                "Finn Jones",
                                "Danny Rand",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Jessica Henwick",
                                "Colleen Wing",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Jessica Stroup",
                                "Joy Meachum",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Tom Pelphrey",
                                "Ward Meachum",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        13,
                                        "55m",
                                        "2017",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        10,
                                        "55m",
                                        "2018",
                                        true
                                )
                        )
                )
        )
    }

    fun saved() : MutableList<MoviesRemote> {
        return mutableListOf(
                MoviesRemote(
                        1, "A Star Is Born", "Movies", "Drama, Romance, Music", "2h 16m", "4.5", "2018", "movie_poster_a_start_is_born.jpg", "Seasoned musician Jackson Maine discovers — and falls in love with — struggling artist Ally. She has just about given up on her dream to make it big as a singer — until Jack coaxes her into the spotlight. But even as Ally's career takes off, the personal side of their relationship is breaking down, as Jack fights an ongoing battle with his own internal demons.", listOf(
                        CastRemote(
                                1,
                                "Jackson Maine",
                                "Bradley Cooper",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Ally Campana",
                                "Lady Gaga",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Sam Elliott",
                                "Bobby Maine",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Andrew Dice Clay",
                                "Lorenzo Campana",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        2, "Game of Thrones", "TV Show", "Sci-Fi & Fantasy, Drama, Action & Adventure, Mystery", "1h", "4.9", "2019", "tvshow_poster_god.jpg", "Seven noble families fight for control of the mythical land of Westeros. Friction between the houses leads to full-scale war. All while a very ancient evil awakens in the farthest north. Amidst the war, a neglected military order of misfits, the Night's Watch, is all that stands between the realms of men and icy horrors beyond.", listOf(
                        CastRemote(
                                1,
                                "Emilia Clarke",
                                "Daenerys Targaryen",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                2,
                                "Kit Harington",
                                "Jon Snow",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Sophie Turner",
                                "Sansa Stark",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Peter Dinklage",
                                "Tyrion Lannister",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        10,
                                        "1h",
                                        "2011",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        10,
                                        "1h",
                                        "2012",
                                        false
                                ),
                                ChapterRemote(
                                        3,
                                        "Season 3",
                                        10,
                                        "1h",
                                        "2013",
                                        false
                                ),
                                ChapterRemote(
                                        4,
                                        "Season 4",
                                        10,
                                        "1h",
                                        "2014",
                                        false
                                ),
                                ChapterRemote(
                                        5,
                                        "Season 5",
                                        10,
                                        "1h",
                                        "2015",
                                        false
                                ),
                                ChapterRemote(
                                        6,
                                        "Season 6",
                                        10,
                                        "1h",
                                        "2016",
                                        false
                                ),
                                ChapterRemote(
                                        7,
                                        "Season 7",
                                        7,
                                        "1h",
                                        "2017",
                                        false
                                ),
                                ChapterRemote(
                                        8,
                                        "Season 8",
                                        6,
                                        "1h",
                                        "2019",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        3, "Aquaman", "Movies", "Action, Adventure, Fantasy", "2h 24m", "4.1", "2018", "movie_poster_aquaman.jpg", "Once home to the most advanced civilization on Earth, Atlantis is now an underwater kingdom ruled by the power-hungry King Orm. With a vast army at his disposal, Orm plans to conquer the remaining oceanic people and then the surface world. Standing in his way is Arthur Curry, Orm's half-human, half-Atlantean brother and true heir to the throne.", listOf(
                        CastRemote(
                                1,
                                "Jason Momoa",
                                "Aquaman",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Amber Heard",
                                "Mera",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Willem Dafoe",
                                "Nuidis Vulko",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Patrick Wilson",
                                "Ocean Master",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        4, "Bohemian Rhapsody", "Movies", "Drama, Music", "2h 15m", "4.8", "2018", "movie_poster_bohemian.jpg", "Singer Freddie Mercury, guitarist Brian May, drummer Roger Taylor and bass guitarist John Deacon take the music world by storm when they form the rock 'n' roll band Queen in 1970. Hit songs become instant classics. When Mercury's increasingly wild lifestyle starts to spiral out of control, Queen soon faces its greatest challenge yet – finding a way to keep the band together amid the success and excess.", listOf(
                        CastRemote(
                                1,
                                "Rami Malek",
                                "Freddie Mercury",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Lucy Boynton",
                                "Mary Austin",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Gwilym Lee",
                                "Brian May",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Ben Hardy",
                                "Roger Taylor",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        5, "Cold Pursuit", "Movies", "Action, Crime, Thriller", "1h 59m", "4.0", "2019", "movie_poster_cold_persuit.jpg", "The quiet family life of Nels Coxman, a snowplow driver, is upended after his son's murder. Nels begins a vengeful hunt for Viking, the drug lord he holds responsible for the killing, eliminating Viking's associates one by one. As Nels draws closer to Viking, his actions bring even more unexpected and violent consequences, as he proves that revenge is all in the execution.", listOf(
                        CastRemote(
                                1,
                                "Liam Neeson",
                                "Nels Coxman",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Laura Dern",
                                "Grace Coxman",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Emmy Rossum",
                                "Kim Dash",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Michael Eklund",
                                "Speedo",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        6, "Doom Patrol", "TV Show", "Sci-Fi & Fantasy, Action & Adventure", "49m", "4.6", "2020", "tvshow_poster_doom_patrol.jpg", "The Doom Patrol’s members each suffered horrible accidents that gave them superhuman abilities — but also left them scarred and disfigured. Traumatized and downtrodden, the team found purpose through The Chief, who brought them together to investigate the weirdest phenomena in existence — and to protect Earth from what they find.", listOf(
                        CastRemote(
                                1,
                                "Diane Guerrero",
                                "Crazy Jane",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                2,
                                "April Bowlby",
                                "Elasti-Woman",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Joivan Wade",
                                "Cyborg",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Matt Bomer",
                                "Negative Man",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        15,
                                        "49m",
                                        "2019",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        9,
                                        "49m",
                                        "2020",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        7, "Hanna", "TV Show", "Action & Adventure, Drama", "50m", "4.4", "2020", "tvshow_poster_hanna.jpg", "This thriller and coming-of-age drama follows the journey of an extraordinary young girl as she evades the relentless pursuit of an off-book CIA agent and tries to unearth the truth behind who she is. Based on the 2011 Joe Wright film.", listOf(
                        CastRemote(
                                1,
                                "Esme Creed-Miles",
                                "Hanna",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                2,
                                "Mireille Enos",
                                "Marissa",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Noah Taylor",
                                "Dr. Roland Kunek",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                4,
                                "Joel Kinnaman",
                                "Erik Heller",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        8,
                                        "50m",
                                        "2019",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        8,
                                        "50m",
                                        "2020",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        8, "Alita: Battle Angel", "Movies", "Action, Science Fiction, Adventure", "2h 2m", "4.2", "2019", "movie_poster_alita.jpg", "When Alita awakens with no memory of who she is in a future world she does not recognize, she is taken in by Ido, a compassionate doctor who realizes that somewhere in this abandoned cyborg shell is the heart and soul of a young woman with an extraordinary past.", listOf(
                        CastRemote(
                                1,
                                "Rosa Salazar",
                                "Alita",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                2,
                                "Christoph Waltz",
                                "Dr. Dyson Ido",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Jennifer Connelly",
                                "Chiren",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Mahershala Ali",
                                "Vector",
                                "img_actor.jpg"
                        )
                ),
                        null
                ),
                MoviesRemote(
                        9, "Gotham", "TV Show", "Drama, Fantasy, Crime", "43m", "4.5", "2019", "tvshow_poster_gotham.jpg", "Everyone knows the name Commissioner Gordon. He is one of the crime world's greatest foes, a man whose reputation is synonymous with law and order. But what is known of Gordon's story and his rise from rookie detective to Police Commissioner? What did it take to navigate the multiple layers of corruption that secretly ruled Gotham City, the spawning ground of the world's most iconic villains? And what circumstances created them – the larger-than-life personas who would become Catwoman, The Penguin, The Riddler, Two-Face and The Joker?", listOf(
                        CastRemote(
                                1,
                                "Ben McKenzie",
                                "James Gordon",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Donal Logue",
                                "Harvey Bullock",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                3,
                                "Erin Richards",
                                "Barbara Kean",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "David Mazouz",
                                "Batman",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        22,
                                        "43m",
                                        "2014",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 1",
                                        22,
                                        "43m",
                                        "2015",
                                        false
                                ),
                                ChapterRemote(
                                        3,
                                        "Season 3",
                                        22,
                                        "43m",
                                        "2016",
                                        false
                                ),
                                ChapterRemote(
                                        4,
                                        "Season 4",
                                        22,
                                        "43m",
                                        "2017",
                                        false
                                ),
                                ChapterRemote(
                                        5,
                                        "Season 5",
                                        12,
                                        "43m",
                                        "2019",
                                        true
                                )
                        )
                ),
                MoviesRemote(
                        10, "Marvel's Iron Fist", "TV Show", "Action & Adventure, Drama, Sci-Fi & Fantasy", "55m", "4.2", "2018", "tvshow_poster_iron_fist.jpg", "Danny Rand resurfaces 15 years after being presumed dead. Now, with the power of the Iron Fist, he seeks to reclaim his past and fulfill his destiny.", listOf(
                        CastRemote(
                                1,
                                "Finn Jones",
                                "Danny Rand",
                                "img_actor.jpg"
                        ),
                        CastRemote(
                                2,
                                "Jessica Henwick",
                                "Colleen Wing",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                3,
                                "Jessica Stroup",
                                "Joy Meachum",
                                "img_actor_women.jpg"
                        ),
                        CastRemote(
                                4,
                                "Tom Pelphrey",
                                "Ward Meachum",
                                "img_actor.jpg"
                        )
                ),
                        listOf(
                                ChapterRemote(
                                        1,
                                        "Season 1",
                                        13,
                                        "55m",
                                        "2017",
                                        false
                                ),
                                ChapterRemote(
                                        2,
                                        "Season 2",
                                        10,
                                        "55m",
                                        "2018",
                                        true
                                )
                        )
                )
        )
    }
}