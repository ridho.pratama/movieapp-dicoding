package com.application.idong.moviecatalogapps.ui.movies

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.application.idong.moviecatalogapps.data.local.entity.Movies
import com.application.idong.moviecatalogapps.databinding.FragmentMoviesBinding
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote
import com.application.idong.moviecatalogapps.factory.ViewModelFactory
import com.application.idong.moviecatalogapps.vo.Resource
import com.application.idong.moviecatalogapps.utils.ViewUtil

/**
 * Created by ridhopratama on 07,December,2020
 */

class MoviesFragment : Fragment(), MovieListener {

    private lateinit var binding: FragmentMoviesBinding
    private lateinit var viewModel: MoviesViewModel
    private lateinit var adapterMovies: MoviesAdapter
    private var position = 0

    companion object {

        const val ARG_POSITION = "position"

        fun newInstance(position: Int) : MoviesFragment {
            val fragment = MoviesFragment()
            fragment.arguments = bundleOf(ARG_POSITION to position)
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMoviesBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments != null) {
            position = arguments?.getInt(ARG_POSITION, 0) as Int
        }
        loadPopularAndNewestMovie()
    }

    private fun loadPopularAndNewestMovie() {
        val factory = ViewModelFactory.getInstance(requireActivity())
        viewModel = ViewModelProvider(this, factory).get(MoviesViewModel::class.java).apply {
            moviewViewState.observe(viewLifecycleOwner, Observer(this@MoviesFragment::handleState))
            getMovies(position)
        }
        adapterMovies = MoviesAdapter(this)
        with(binding.rvMovies) {
            setHasFixedSize(true)
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(requireContext())
            adapter = adapterMovies
        }
    }

    private fun handleState(viewState: Resource<List<MoviesRemote>>?) {
        viewState?.let { response ->
            when(response) {
                is Resource.Loading -> toggleLoading(true)
                is Resource.Success -> {
                    toggleLoading(false)
                    response.data?.let {
                        showData(it)
                    }
                }
                is Resource.Error -> {
                    toggleLoading(false)
                    response.message?.let {
                        showError(it)
                    }
                }
            }
        }
    }

    private fun  toggleLoading(loading: Boolean) {
        if (loading) {
            binding.smFragmentMovies.startShimmer()
            binding.smFragmentMovies.visibility = View.VISIBLE
        }
        else {
            binding.smFragmentMovies.stopShimmer()
            binding.smFragmentMovies.visibility = View.GONE
        }
    }

    private fun showData(data: List<MoviesRemote>) {
        binding.rvMovies.visibility = View.VISIBLE
        adapterMovies.updateData(data.toMutableList())
    }

    private fun showError(error: String) {
        ViewUtil.showToast(requireContext(), error)
    }

    override fun selectMovie(movies: MoviesRemote) {
        val intent = Intent(requireContext(), DetailMovieActivity::class.java)
        intent.putExtra(DetailMovieActivity.EXTRA_CATEGORY, position)
        intent.putExtra(DetailMovieActivity.EXTRA_MOVIEID, movies.id)
        startActivity(intent)
    }

    override fun selectMovieOnHome(type: String, movie: MoviesRemote) {

    }

    override fun selectFavoriteMovie(movie: Movies) {

    }

}