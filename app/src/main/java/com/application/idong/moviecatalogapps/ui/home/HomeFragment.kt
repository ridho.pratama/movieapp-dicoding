package com.application.idong.moviecatalogapps.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.application.idong.moviecatalogapps.ui.movies.DetailMovieActivity
import com.application.idong.moviecatalogapps.ui.movies.MoviesActivity
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.data.local.entity.Movies
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote
import com.application.idong.moviecatalogapps.databinding.FragmentHomeBinding
import com.application.idong.moviecatalogapps.ui.movies.MovieListener
import com.application.idong.moviecatalogapps.factory.ViewModelFactory
import com.application.idong.moviecatalogapps.utils.Menu
import com.application.idong.moviecatalogapps.vo.Resource
import com.application.idong.moviecatalogapps.utils.ViewUtil
import com.facebook.shimmer.ShimmerFrameLayout

/**
 * Created by ridhopratama on 06,December,2020
 */

class HomeFragment : Fragment(), CategoryListener, MovieListener {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var viewModel: HomeViewModel
    private lateinit var adapterPopular: PopularMoviesHomeAdapter
    private lateinit var adapterNewest: PopularMoviesHomeAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupActionClick()
        loadCategory()
        loadPopularAndNewestMovie()
    }

    private fun setupActionClick() {
        with(binding) {
            tvSeeAllPopularMovies.setOnClickListener {
                findNavController().navigate(R.id.action_homeFragment_to_moviesActivity)
            }
            tvSeeAllNewestMovies.setOnClickListener {
                findNavController().navigate(R.id.action_homeFragment_to_moviesActivity)
            }
        }
    }

    private fun loadCategory() {
        val adapter = CategoryHomeAdapter(this)
        adapter.updateData(Menu.categorySample())
        binding.apply {
            rvCategory.layoutManager = GridLayoutManager(requireContext(), 2)
            rvCategory.setHasFixedSize(true)
            rvCategory.adapter = adapter
        }
    }

    private fun loadPopularAndNewestMovie() {
        val factory = ViewModelFactory.getInstance(requireActivity())
        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java).apply {
            popularViewState.observe(viewLifecycleOwner, Observer(this@HomeFragment::handleStatePopular))
            newestViewState.observe(viewLifecycleOwner, Observer(this@HomeFragment::handleStateNewest))
            getPopularMovies()
            getNewestMovies()
        }
        adapterPopular = PopularMoviesHomeAdapter(getString(R.string.title_popular_movies),this)
        with(binding.rvPopularMovies) {
            overScrollMode = View.OVER_SCROLL_NEVER
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = adapterPopular
        }

        adapterNewest = PopularMoviesHomeAdapter(getString(R.string.title_newest_movies),this)
        with(binding.rvNewestMovies) {
            overScrollMode = View.OVER_SCROLL_NEVER
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = adapterNewest
        }
    }

    private fun handleStatePopular(viewState: Resource<List<MoviesRemote>>?) {
        viewState?.let { response ->
            when(response) {
                is Resource.Loading -> toggleLoading(true, binding.smPopularMovies)
                is Resource.Success -> {
                    toggleLoading(false, binding.smPopularMovies)
                    response.data?.let {
                        showData(getString(R.string.title_popular_movies), it)
                    }
                }
                is Resource.Error -> {
                    toggleLoading(false, binding.smPopularMovies)
                    response.message?.let {
                        showError(it)
                    }
                }
            }
        }
    }

    private fun handleStateNewest(viewState: Resource<List<MoviesRemote>>?) {
        viewState?.let { response ->
            when(response) {
                is Resource.Loading -> toggleLoading(true, binding.smNewestMovies)
                is Resource.Success -> {
                    toggleLoading(false, binding.smNewestMovies)
                    response.data?.let {
                        showData(getString(R.string.title_newest_movies), it)
                    }
                }
                is Resource.Error -> {
                    toggleLoading(false, binding.smNewestMovies)
                    response.message?.let {
                        showError(it)
                    }
                }
            }
        }
    }

    private fun  toggleLoading(loading: Boolean, shimmerFrameLayout: ShimmerFrameLayout) {
        if (loading) {
            shimmerFrameLayout.startShimmer()
            shimmerFrameLayout.visibility = View.VISIBLE
        }
        else {
            shimmerFrameLayout.stopShimmer()
            shimmerFrameLayout.visibility = View.GONE
        }
    }

    private fun showData(type: String, data: List<MoviesRemote>) {
        when(type) {
            getString(R.string.title_popular_movies) -> {
                binding.rvPopularMovies.visibility = View.VISIBLE
                adapterPopular.updateData(data.toMutableList())
            }
            else -> {
                binding.rvNewestMovies.visibility = View.VISIBLE
                adapterNewest.updateData(data.toMutableList())
            }
        }
    }

    private fun showError(error: String) {
        ViewUtil.showToast(requireContext(), error)
    }

    override fun onSelectCategory(position: Int) {
        findNavController().navigate(R.id.action_homeFragment_to_moviesActivity, bundleOf(MoviesActivity.EXTRA_CATEGORY to position))
    }

    override fun selectMovie(movie: MoviesRemote) {

    }

    override fun selectMovieOnHome(type: String, movie: MoviesRemote) {
        val intent = Intent(requireContext(), DetailMovieActivity::class.java)
        val category = when(type) {
            getString(R.string.title_popular_movies) -> 2
            else -> 3
        }
        intent.putExtra(DetailMovieActivity.EXTRA_CATEGORY, category)
        intent.putExtra(DetailMovieActivity.EXTRA_MOVIEID, movie.id)
        startActivity(intent)
    }

    override fun selectFavoriteMovie(movie: Movies) {

    }
}