package com.application.idong.moviecatalogapps.ui.saved

import android.graphics.PorterDuff
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.application.idong.moviecatalogapps.api.UriService
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.ui.movies.CastMoviesAdapter
import com.application.idong.moviecatalogapps.ui.movies.ChapterMoviesAdapter
import com.application.idong.moviecatalogapps.data.local.entity.Movies
import com.application.idong.moviecatalogapps.ui.movies.ChapterListener
import com.application.idong.moviecatalogapps.data.remote.entity.ChapterRemote
import com.application.idong.moviecatalogapps.databinding.ActivityDetailMovieSavedBinding
import com.application.idong.moviecatalogapps.factory.ViewModelFactory
import com.application.idong.moviecatalogapps.utils.ConvertObject
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.appbar.AppBarLayout
import kotlin.math.abs

class DetailMovieSavedActivity : AppCompatActivity(), ChapterListener {

    private lateinit var viewModel: SavedViewModel
    private lateinit var adapterCast: CastMoviesAdapter
    private lateinit var adapterChapter: ChapterMoviesAdapter

    companion object {
        const val EXTRA_MOVIEID = "movieId"
    }

    private lateinit var binding: ActivityDetailMovieSavedBinding
    private var movieId: Int = 0
    private var movies: Movies? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailMovieSavedBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupToolbar()
        val bundle = intent.extras
        if (bundle != null) {
            movieId = bundle.getInt(EXTRA_MOVIEID)
        }
        loadCast()
        loadChapter()
        loadData()
    }

    private fun setupToolbar() {
        with(binding.tbPartialCollapsing) {
            btnBack.setOnClickListener {
                onBackPressed()
            }
        }
        with(binding.ablDetailMovie) {
               addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
                   when {
                       abs(verticalOffset) == appBarLayout.totalScrollRange -> {
                           window.statusBarColor = ContextCompat.getColor(this@DetailMovieSavedActivity, R.color.colorWhite)
                       }
                       verticalOffset == 0 -> {
                           window.statusBarColor = ContextCompat.getColor(this@DetailMovieSavedActivity, android.R.color.transparent)
                       }
                       else -> {
                           if (abs(verticalOffset) > appBarLayout.totalScrollRange / 2) {
                               window.statusBarColor = ContextCompat.getColor(this@DetailMovieSavedActivity, R.color.colorWhite)
                               binding.cvActionTop.visibility = View.GONE
                           }
                           else {
                               window.statusBarColor = ContextCompat.getColor(this@DetailMovieSavedActivity, android.R.color.transparent)
                               binding.cvActionTop.visibility = View.VISIBLE
                           }
                       }
                   }
               })
        }
    }

    private fun loadData() {
        val factory = ViewModelFactory.getInstance(this)
        viewModel = ViewModelProvider(this, factory)[SavedViewModel::class.java]
        viewModel.getFavoriteMoviesById(movieId).observe(this, { movie ->
            showData(movie)
        })
    }

    private fun showData(movies: Movies?) {
        if (movies == null) {
            onBackPressed()
        }

        movies?.let {
            this.movies = movies
            binding.loading.visibility = View.GONE
            binding.contentDetail.root.visibility = View.VISIBLE

            Glide.with(this)
                    .load(UriService.POSTER_URL + movies.poster)
                    .apply(
                            RequestOptions.placeholderOf(R.drawable.img_loading)
                                    .error(R.drawable.img_error))
                    .into(binding.ivPoster)
            binding.tvRating.text = movies.rating

            with(binding.contentDetail) {
                var category = ""
                if (movies.category.equals(getString(R.string.title_tvshow), true)) {
                    category = resources.getString(R.string.title_detail_category_tvshow, movies.category, movies.chapter?.size )
                    tvCategory.setBackgroundResource(R.drawable.shape_roundcorner_primary)
                } else {
                    category = movies.category.toString()
                    tvCategory.setBackgroundResource(R.drawable.shape_roundcorner_accent)
                }
                tvCategory.text = category
                tvTitleMovie.text = movies.title
                tvYear.text = movies.year
                tvTime.text = movies.time
                tvGenre.text = movies.genre
                tvSummary.text = movies.summary
            }

            adapterCast.updateData(ConvertObject.collectionCastToRemoteCast(movies.cast).toMutableList())
            if (movies.category.equals(getString(R.string.title_tvshow), true)) {
                binding.contentDetail.llChapter.visibility = View.VISIBLE
                movies.chapter?.let { chapter ->
                    ConvertObject.collectionChapterToChapterRemote(chapter)?.let {
                        chapterRemote -> adapterChapter.updateData(chapterRemote.toMutableList())
                    }
                }
            }

            setFavoriteMovie()
        }
    }

    private fun loadCast() {
        adapterCast = CastMoviesAdapter()
        binding.contentDetail.rvCast.apply {
            overScrollMode = View.OVER_SCROLL_NEVER
            layoutManager = LinearLayoutManager(this@DetailMovieSavedActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = adapterCast
            ViewCompat.setNestedScrollingEnabled(this, false)
        }
    }

    private fun loadChapter() {
        adapterChapter = ChapterMoviesAdapter(this)
        binding.contentDetail.rvChapter.apply {
            layoutManager = GridLayoutManager(this@DetailMovieSavedActivity, 4)
            setHasFixedSize(true)
            adapter = adapterChapter
            ViewCompat.setNestedScrollingEnabled(this, false)
        }
    }

    private fun setFavoriteMovie() {
        binding.ivSaved.apply {
            setImageResource(R.drawable.ic_bookmark)
            setColorFilter(
                    ContextCompat.getColor(this@DetailMovieSavedActivity, R.color.colorAccent),
                    PorterDuff.Mode.SRC_ATOP
            )
        }
        binding.tvSaved.text = getString(R.string.title_saved_favorite)
        binding.llBookmarkDetailSaved.setOnClickListener {
            deleteFavorite()
        }
    }

    private fun deleteFavorite() {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle("Delete favorite movie")
        alertDialogBuilder
                .setMessage(getString(R.string.title_delete_favorite_confirm))
                .setCancelable(false)
                .setPositiveButton("Yes") { dialog, id ->
                    this.movies?.let { movie ->
                        viewModel.delete(movie)
                    }
                }
                .setNegativeButton("No") {
                    dialog, id -> dialog.cancel()
                }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }

    override fun onSelectChapter(chapter: ChapterRemote) {
        with(binding.contentDetail) {
            tvTitleMovie.text = "${movies?.title} - ${chapter.chapterName}"
            tvYear.text = chapter.chapterYear
        }
    }
}