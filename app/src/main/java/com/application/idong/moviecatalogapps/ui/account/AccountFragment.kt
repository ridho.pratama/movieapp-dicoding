package com.application.idong.moviecatalogapps.ui.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.databinding.FragmentAccountBinding

/**
 * Created by ridhopratama on 06,December,2020
 */

class AccountFragment : Fragment() {

    private lateinit var binding: FragmentAccountBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAccountBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        loadMenu()
    }

    private fun setupToolbar() {
        with(binding.toolbar) {
            btnBack.visibility = View.GONE
            tvTitle.text = getString(R.string.title_account)
        }
    }

    private fun loadMenu() {
        val viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory())[AccountViewModel::class.java]
        val accountMenus = viewModel.getAccountMenu()
        val adapterAccountMenu = AccountMenuAdapter()
        adapterAccountMenu.updateData(accountMenus)
        with(binding.rvAccountMenu) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = adapterAccountMenu
        }
    }

}