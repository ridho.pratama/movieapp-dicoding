package com.application.idong.moviecatalogapps.ui.account

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.models.AccountMenu

/**
 * Created by ridhopratama on 06,December,2020
 */

class AccountMenuAdapter : RecyclerView.Adapter<AccountMenuAdapter.ViewHolder>() {

    private val accountMenus = mutableListOf<AccountMenu>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_account_menu, parent, false)
        )

    override fun getItemCount(): Int = accountMenus.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = accountMenus[position]
        holder.apply {
            ivIconMenu.setImageResource(data.iconMenu)
            tvNameMenu.text = data.labelMenu
        }
    }

    fun updateData(newAccountMenus: MutableList<AccountMenu>) {
        accountMenus.clear()
        accountMenus.addAll(newAccountMenus)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var ivIconMenu: ImageView = itemView.findViewById(R.id.ivIconMenu)
        internal var tvNameMenu: TextView = itemView.findViewById(R.id.tvNameMenu)
    }
}