package com.application.idong.moviecatalogapps.ui

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.ui.bottomnavigation.BottombarAdapter
import com.application.idong.moviecatalogapps.databinding.ActivityMainBinding
import com.application.idong.moviecatalogapps.models.BottombarModel
import com.application.idong.moviecatalogapps.ui.bottomnavigation.Bottombar
import com.application.idong.moviecatalogapps.utils.Menu


class MainActivity : AppCompatActivity(), BottombarAdapter.ItemSelectorInterface {

    private lateinit var binding: ActivityMainBinding
    private lateinit var bottombar: Bottombar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initializeView()
    }

    @SuppressLint("ResourceType")
    private fun initializeView() {
        bottombar = Bottombar(this, binding.customBottomBar.root, this)
        bottombar.addAllItem(Menu.mainMenu(this))
        bottombar.changeBackground(getString(R.color.colorWhite))
        bottombar.setDefaultBackground(getString(R.color.colorWhite))
        bottombar.setDefaultTint(getString(R.color.colorBlack))
        bottombar.changeDividerColor(getString(R.color.colorBackgroundGrey))
        bottombar.apply(0)
    }

    override fun itemSelected(bottombarModel: BottombarModel) {
        when (bottombarModel.itemId) {
            0 -> findNavController(R.id.nav_host_fragment).navigate(R.id.homeFragment)
            1 -> findNavController(R.id.nav_host_fragment).navigate(R.id.savedFragment)
            2 -> findNavController(R.id.nav_host_fragment).navigate(R.id.accountFragment)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}