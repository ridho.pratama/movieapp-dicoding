package com.application.idong.moviecatalogapps.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.application.idong.moviecatalogapps.data.MovieRepository
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote
import com.application.idong.moviecatalogapps.utils.Constant
import com.application.idong.moviecatalogapps.vo.Resource
import kotlinx.coroutines.launch

/**
 * Created by ridhopratama on 07,December,2020
 */

class HomeViewModel(private val movieRepository: MovieRepository): ViewModel() {

    private val mPopularViewState = MutableLiveData<Resource<List<MoviesRemote>>>()

    val popularViewState: MutableLiveData<Resource<List<MoviesRemote>>>
        get() = mPopularViewState

    private val mNewestViewState = MutableLiveData<Resource<List<MoviesRemote>>>()

    val newestViewState: MutableLiveData<Resource<List<MoviesRemote>>>
        get() = mNewestViewState

    fun getPopularMovies() = viewModelScope.launch {
        mPopularViewState.postValue(Resource.Loading())
        try {
            val movies = movieRepository.getRemotePopular()
            mPopularViewState.postValue(Resource.Success(movies))
        }
        catch (ex: Exception) {
            mPopularViewState.postValue(Resource.Error(ex.message ?: Constant.MESSAGE_SERVERERROR))
        }
    }

    fun getNewestMovies() = viewModelScope.launch {
        mNewestViewState.postValue(Resource.Loading())
        try {
            val movies = movieRepository.getRemoteNewest()
            mNewestViewState.postValue(Resource.Success(movies))
        }
        catch (ex: Exception) {
            mNewestViewState.postValue(Resource.Error(ex.message ?: Constant.MESSAGE_SERVERERROR))
        }
    }
}