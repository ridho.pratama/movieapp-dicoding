package com.application.idong.moviecatalogapps.data.local

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import com.application.idong.moviecatalogapps.data.local.database.MovieDao
import com.application.idong.moviecatalogapps.data.local.entity.Movies

/**
 * Created by ridhopratama on 29,December,2020
 */
class LocalDataSource private constructor(private val mMoviesDao: MovieDao){

    companion object {
        @Volatile
        private var instance: LocalDataSource? = null

        fun getInstance(movieDao: MovieDao): LocalDataSource =
            instance ?: synchronized(this) {
                instance ?: LocalDataSource(movieDao)
            }
    }

    fun getFavoriteMovies(category: String): DataSource.Factory<Int, Movies> = mMoviesDao.getFavoriteMovies(category)

    fun getFavoriteMoviesById(moviesId: Int): LiveData<Movies> = mMoviesDao.getFavoriteMoviesById(moviesId)

    suspend fun upsert(movies: Movies) = mMoviesDao.upsert(movies)

    suspend fun delete(movies: Movies) = mMoviesDao.delete(movies)
}