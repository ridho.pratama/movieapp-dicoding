package com.application.idong.moviecatalogapps.data.remote.response

import android.os.Parcelable
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote
import kotlinx.parcelize.Parcelize

/**
 * Created by ridhopratama on 20,December,2020
 */

data class MovieListResponse(
    val status: Int,
    val message: String,
    val data: List<MoviesRemote>?
)
