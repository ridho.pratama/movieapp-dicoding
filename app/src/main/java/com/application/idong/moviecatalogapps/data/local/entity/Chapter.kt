package com.application.idong.moviecatalogapps.data.local.entity

/**
 * Created by ridhopratama on 07,December,2020
 */
data class Chapter(
    val id: Int,
    val chapterName: String,
    val chapterTotal: Int,
    val chapterTime: String,
    val chapterYear: String,
    val isCurrent: Boolean
)