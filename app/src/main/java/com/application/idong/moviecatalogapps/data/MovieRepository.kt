package com.application.idong.moviecatalogapps.data

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.application.idong.moviecatalogapps.data.local.LocalDataSource
import com.application.idong.moviecatalogapps.data.local.entity.Movies
import com.application.idong.moviecatalogapps.data.remote.RemoteDataSource
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote

/**
 * Created by ridhopratama on 20,December,2020
 */

class MovieRepository private constructor(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
    ): MoviesDataSource {

    companion object {
        @Volatile
        private var instance: MovieRepository? = null

        fun getInstance(remoteData: RemoteDataSource, localData: LocalDataSource): MovieRepository =
            instance ?: synchronized(this) {
                instance ?: MovieRepository(remoteData, localData)
            }
    }

    override suspend fun getRemotePopular(): List<MoviesRemote> {
        return remoteDataSource.getPopular()
    }

    override suspend fun getRemoteNewest(): List<MoviesRemote> {
        return remoteDataSource.getNewest()
    }

    override suspend fun getRemoteMovies(): List<MoviesRemote> {
        return remoteDataSource.getMovies()
    }

    override suspend fun getRemoteTvshow(): List<MoviesRemote> {
        return remoteDataSource.getTvShow()
    }

    override suspend fun getRemoteSaved(): List<MoviesRemote> {
        return remoteDataSource.getSaved()
    }

    override suspend fun getRemotePopularDetail(id: Int): MoviesRemote {
        return remoteDataSource.getPopularDetail(id)
    }

    override suspend fun getRemoteNewestDetail(id: Int): MoviesRemote {
        return remoteDataSource.getNewestDetail(id)
    }

    override suspend fun getRemoteMoviesDetail(id: Int): MoviesRemote {
        return remoteDataSource.getMoviesDetail(id)
    }

    override suspend fun getRemoteTvshowDetail(id: Int): MoviesRemote {
        return remoteDataSource.getTvShowDetail(id)
    }

    override suspend fun getRemoteSavedDetail(id: Int): MoviesRemote {
        return remoteDataSource.getSavedDetail(id)
    }

    override fun getFavoriteMovies(category: String): LiveData<PagedList<Movies>> {
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(4)
            .setPageSize(4)
            .build()

        return LivePagedListBuilder(localDataSource.getFavoriteMovies(category), config).build()
    }

    override fun getFavoriteMoviesById(id: Int): LiveData<Movies> {
        return localDataSource.getFavoriteMoviesById(id)
    }

    override suspend fun upsert(movies: Movies) {
        localDataSource.upsert(movies)
    }

    override suspend fun delete(movies: Movies) {
        localDataSource.delete(movies)
    }

}