package com.application.idong.moviecatalogapps.ui.movies

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.application.idong.moviecatalogapps.models.Category
import com.application.idong.moviecatalogapps.ui.saved.SavedFragment

/**
 * Created by ridhopratama on 07,December,2020
 */

class ViewPagerFragmentAdapter(private val context: Context, fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    var isSaved = false
    var categories = mutableListOf<Category>()

    override fun createFragment(pos: Int): Fragment {
        return when {
            isSaved -> SavedFragment.newInstance(categories[pos].id)
            else -> MoviesFragment.newInstance(categories[pos].id)
        }
    }

    override fun getItemCount(): Int = categories.size
}