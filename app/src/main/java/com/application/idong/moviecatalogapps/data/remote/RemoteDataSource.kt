package com.application.idong.moviecatalogapps.data.remote

import com.application.idong.moviecatalogapps.api.ApiClientAdapter
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote
import com.application.idong.moviecatalogapps.utils.EspressoIdlingResource

/**
 * Created by ridhopratama on 20,December,2020
 */
class RemoteDataSource private constructor(private val  apiClientAdapter: ApiClientAdapter) {

    companion object {

        @Volatile
        private var instance: RemoteDataSource? = null

        fun getInstance(apiClient: ApiClientAdapter): RemoteDataSource =
            instance ?: synchronized(this) {
                instance ?: RemoteDataSource(apiClient)
            }
    }

    suspend fun getPopular(): List<MoviesRemote> {
        EspressoIdlingResource.increment()
        val response = apiClientAdapter.getMovieService().popular()
        if (response.isSuccessful) {
            EspressoIdlingResource.decrement()
            return response.body()?.data as List<MoviesRemote>
        }
        throw Exception("Terjadi kesalahan saat melakukan request data, status error ${response.code()}")
    }

    suspend fun getNewest(): List<MoviesRemote> {
        EspressoIdlingResource.increment()
        val response = apiClientAdapter.getMovieService().newest()
        if (response.isSuccessful) {
            EspressoIdlingResource.decrement()
            return response.body()?.data as List<MoviesRemote>
        }
        throw Exception("Terjadi kesalahan saat melakukan request data, status error ${response.code()}")
    }

    suspend fun getMovies(): List<MoviesRemote> {
        EspressoIdlingResource.increment()
        val response = apiClientAdapter.getMovieService().movies()
        if (response.isSuccessful) {
            EspressoIdlingResource.decrement()
            return response.body()?.data as List<MoviesRemote>
        }
        throw Exception("Terjadi kesalahan saat melakukan request data, status error ${response.code()}")
    }

    suspend fun getTvShow(): List<MoviesRemote> {
        EspressoIdlingResource.increment()
        val response = apiClientAdapter.getMovieService().tvShow()
        if (response.isSuccessful) {
            EspressoIdlingResource.decrement()
            return response.body()?.data as List<MoviesRemote>
        }
        throw Exception("Terjadi kesalahan saat melakukan request data, status error ${response.code()}")
    }

    suspend fun getSaved(): List<MoviesRemote> {
        EspressoIdlingResource.increment()
        val response = apiClientAdapter.getMovieService().saved()
        if (response.isSuccessful) {
            EspressoIdlingResource.decrement()
            return response.body()?.data as List<MoviesRemote>
        }
        throw Exception("Terjadi kesalahan saat melakukan request data, status error ${response.code()}")
    }

    suspend fun getPopularDetail(id: Int): MoviesRemote {
        EspressoIdlingResource.increment()
        val response = apiClientAdapter.getMovieService().popularDetail(id)
        if (response.isSuccessful) {
            EspressoIdlingResource.decrement()
            return response.body()?.data as MoviesRemote
        }
        throw Exception("Terjadi kesalahan saat melakukan request data, status error ${response.code()}")
    }

    suspend fun getNewestDetail(id: Int): MoviesRemote {
        EspressoIdlingResource.increment()
        val response = apiClientAdapter.getMovieService().newestDetail(id)
        if (response.isSuccessful) {
            EspressoIdlingResource.decrement()
            return response.body()?.data as MoviesRemote
        }
        throw Exception("Terjadi kesalahan saat melakukan request data, status error ${response.code()}")
    }

    suspend fun getMoviesDetail(id: Int): MoviesRemote {
        EspressoIdlingResource.increment()
        val response = apiClientAdapter.getMovieService().moviesDetail(id)
        if (response.isSuccessful) {
            EspressoIdlingResource.decrement()
            return response.body()?.data as MoviesRemote
        }
        throw Exception("Terjadi kesalahan saat melakukan request data, status error ${response.code()}")
    }

    suspend fun getTvShowDetail(id: Int): MoviesRemote {
        EspressoIdlingResource.increment()
        val response = apiClientAdapter.getMovieService().tvshowDetail(id)
        if (response.isSuccessful) {
            EspressoIdlingResource.decrement()
            return response.body()?.data as MoviesRemote
        }
        throw Exception("Terjadi kesalahan saat melakukan request data, status error ${response.code()}")
    }

    suspend fun getSavedDetail(id: Int): MoviesRemote {
        EspressoIdlingResource.increment()
        val response = apiClientAdapter.getMovieService().savedDetail(id)
        if (response.isSuccessful) {
            EspressoIdlingResource.decrement()
            return response.body()?.data as MoviesRemote
        }
        throw Exception("Terjadi kesalahan saat melakukan request data, status error ${response.code()}")
    }

}