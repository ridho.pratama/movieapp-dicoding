package com.application.idong.moviecatalogapps.ui.movies

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.application.idong.moviecatalogapps.data.MovieRepository
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote
import com.application.idong.moviecatalogapps.utils.Constant
import com.application.idong.moviecatalogapps.vo.Resource
import kotlinx.coroutines.launch

/**
 * Created by ridhopratama on 07,December,2020
 */

class MoviesViewModel(private val movieRepository: MovieRepository): ViewModel() {

    private val mMoviesViewState = MutableLiveData<Resource<List<MoviesRemote>>>()

    val moviewViewState: MutableLiveData<Resource<List<MoviesRemote>>>
        get() = mMoviesViewState

    private val mMoviesDetailViewState = MutableLiveData<Resource<MoviesRemote>>()

    val movieDetailViewState: MutableLiveData<Resource<MoviesRemote>>
        get() = mMoviesDetailViewState


    fun getMovies(categoryId: Int) = viewModelScope.launch {
        mMoviesViewState.postValue(Resource.Loading())
        try {
            val movies = when(categoryId) {
                0 -> movieRepository.getRemoteMovies()
                else -> movieRepository.getRemoteTvshow()
            }

            mMoviesViewState.postValue(Resource.Success(movies))
        }
        catch (ex: Exception) {
            mMoviesViewState.postValue(Resource.Error(ex.message ?: Constant.MESSAGE_SERVERERROR))
        }
    }

    fun getDetailMovies(categoryId: Int, movieId: Int) = viewModelScope.launch {
        mMoviesDetailViewState.postValue(Resource.Loading())
        try {
            val movies = when(categoryId) {
                0 -> movieRepository.getRemoteMoviesDetail(movieId)
                1 -> movieRepository.getRemoteTvshowDetail(movieId)
                2 -> movieRepository.getRemotePopularDetail(movieId)
                3 -> movieRepository.getRemoteNewestDetail(movieId)
                else -> movieRepository.getRemoteSavedDetail(movieId)
            }

            mMoviesDetailViewState.postValue(Resource.Success(movies))
        }
        catch (ex: Exception) {
            mMoviesDetailViewState.postValue(Resource.Error(ex.message ?: Constant.MESSAGE_SERVERERROR))
        }
    }
}