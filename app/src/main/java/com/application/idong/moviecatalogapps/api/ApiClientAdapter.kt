package com.application.idong.moviecatalogapps.api

import com.application.idong.moviecatalogapps.BuildConfig
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by ridhopratama on 20,December,2020
 */

class ApiClientAdapter {
    private val gsonConverterFactory: GsonConverterFactory = GsonConverterFactory.create(Gson())
    private val apiBaseUrl: String = BuildConfig.API_MOVIES
    private val timeout:Long = 90
    private val timeUnit: TimeUnit = TimeUnit.SECONDS

    private fun okHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.HEADERS
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val builder = OkHttpClient.Builder().apply {
            if (BuildConfig.DEBUG) addInterceptor(interceptor)
            connectTimeout(timeout, timeUnit)
            readTimeout(timeout, timeUnit)
            addInterceptor(InterceptorApi())
            authenticator(InterceptorApi())
        }

        return builder.build()
    }

    private fun retrofitInstance(): Retrofit {
        return Retrofit.Builder()
                        .baseUrl(apiBaseUrl)
                        .addConverterFactory(gsonConverterFactory)
                        .client(okHttpClient())
                        .build()
    }

    fun getMovieService(): MoviesService {
        return retrofitInstance().create(MoviesService::class.java)
    }
}