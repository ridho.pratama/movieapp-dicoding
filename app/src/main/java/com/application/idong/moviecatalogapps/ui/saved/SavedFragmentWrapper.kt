package com.application.idong.moviecatalogapps.ui.saved

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.ui.movies.ViewPagerFragmentAdapter
import com.application.idong.moviecatalogapps.databinding.FragmentSavedWrapperBinding
import com.application.idong.moviecatalogapps.utils.Menu
import com.google.android.material.tabs.TabLayoutMediator

/**
 * Created by ridhopratama on 07,December,2020
 */

class SavedFragmentWrapper : Fragment() {

    private lateinit var binding: FragmentSavedWrapperBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSavedWrapperBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        initializeViewPager()
    }

    private fun setupToolbar() {
        with(binding.tbSavedRegular) {
            btnBack.visibility = View.GONE
            tvTitle.text = getString(R.string.title_list_saved_movies)
        }
    }

    private fun initializeViewPager() {
        val viewPagerFragmentAdapter = ViewPagerFragmentAdapter(requireActivity(), childFragmentManager, lifecycle)
        val categories = Menu.categorySample()
        viewPagerFragmentAdapter.isSaved = true
        viewPagerFragmentAdapter.categories = categories
        with(binding) {
            vpSavedWrapper.adapter = viewPagerFragmentAdapter
            vpSavedWrapper.isUserInputEnabled = false
            TabLayoutMediator(tlSavedWrapper, vpSavedWrapper) { tab, pos ->
                tab.text = categories[pos].title
            }.attach()
        }
    }
}