package com.application.idong.moviecatalogapps.ui.movies

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.databinding.ActivityMoviesBinding
import com.application.idong.moviecatalogapps.utils.Menu
import com.google.android.material.tabs.TabLayoutMediator

class MoviesActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMoviesBinding
    private var position: Int = 0

    companion object {
        const val EXTRA_CATEGORY = "category"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMoviesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        toolbar()

        val bundle = intent.extras
        if (bundle != null) {
            position = bundle.getInt(EXTRA_CATEGORY)
        }

        val viewPagerFragmentAdapter = ViewPagerFragmentAdapter(this, supportFragmentManager, lifecycle)
        val categories = Menu.categorySample()
        viewPagerFragmentAdapter.categories = categories
        with(binding) {
            vpWrapperMovie.adapter = viewPagerFragmentAdapter
            TabLayoutMediator(tlWrapperMovie, vpWrapperMovie) { tab, pos ->
                tab.text = categories[pos].title
                vpWrapperMovie.setCurrentItem(position, true)
            }.attach()
        }
    }

    private fun toolbar() {
        with(binding.tbRegular) {
            tvTitle.text = getString(R.string.title_list_movies)
            btnBack.setOnClickListener {
                onBackPressed()
            }
        }
    }

}