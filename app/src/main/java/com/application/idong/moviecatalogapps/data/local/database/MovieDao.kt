package com.application.idong.moviecatalogapps.data.local.database

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import com.application.idong.moviecatalogapps.data.local.entity.Movies

/**
 * Created by ridhopratama on 29,December,2020
 */

@Dao
interface MovieDao {

    @Query("SELECT * FROM movies where category = :category")
    fun getFavoriteMovies(category: String): DataSource.Factory<Int, Movies>

    @Query("SELECT * FROM movies where moviesId = :moviesId")
    fun getFavoriteMoviesById(moviesId: Int) : LiveData<Movies>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(movies: Movies)

    @Delete
    suspend fun delete(movies: Movies)
}