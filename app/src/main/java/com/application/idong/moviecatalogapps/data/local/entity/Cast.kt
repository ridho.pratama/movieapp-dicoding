package com.application.idong.moviecatalogapps.data.local.entity

/**
 * Created by ridhopratama on 07,December,2020
 */
data class Cast(
    val id: Int,
    val name: String,
    val actionAs: String,
    val foto: String
)
