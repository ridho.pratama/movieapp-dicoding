package com.application.idong.moviecatalogapps.data.remote.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * Created by ridhopratama on 07,December,2020
 */

@Parcelize
data class CastRemote(
    val id: Int,
    val name: String,
    val actionAs: String,
    val foto: String
) : Parcelable
