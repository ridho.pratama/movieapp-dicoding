package com.application.idong.moviecatalogapps.api

import com.application.idong.moviecatalogapps.BuildConfig

/**
 * Created by ridhopratama on 20,December,2020
 */

object UriService {
    const val POPULAR = "popular"
    const val NEWEST = "newest"
    const val MOVIES = "movies"
    const val TVSHOW = "tvshow"
    const val SAVED = "saved"
    const val POPULAR_DETAIL = "popular/{id}"
    const val NEWEST_DETAIL = "newest/{id}"
    const val MOVIES_DETAIL = "movies/{id}"
    const val TVSHOW_DETAIL = "tvshow/{id}"
    const val SAVED_DETAIL = "saved/{id}"
    const val POSTER_URL = BuildConfig.API_MOVIES + "poster/"

}