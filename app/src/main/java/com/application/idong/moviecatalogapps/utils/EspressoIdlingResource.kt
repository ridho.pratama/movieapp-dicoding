package com.application.idong.moviecatalogapps.utils

import androidx.test.espresso.idling.CountingIdlingResource

/**
 * Created by ridhopratama on 15,December,2020
 */
object EspressoIdlingResource {
    private const val RESOURCE = "GLOBAL"
    val idlingResource = CountingIdlingResource(RESOURCE)

    fun increment() {
        idlingResource.increment()
    }

    fun decrement() {
        idlingResource.decrement()
    }
}