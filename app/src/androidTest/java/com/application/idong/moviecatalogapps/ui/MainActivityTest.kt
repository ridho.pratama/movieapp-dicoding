package com.application.idong.moviecatalogapps.ui

import android.view.View
import android.widget.HorizontalScrollView
import android.widget.ListView
import android.widget.ScrollView
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import com.application.idong.moviecatalogapps.R
import com.application.idong.moviecatalogapps.data.remote.entity.MoviesRemote
import com.application.idong.moviecatalogapps.utils.DataMoviesRemote
import com.application.idong.moviecatalogapps.utils.EspressoIdlingResource
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit


/**
 * Created by ridhopratama on 11,December,2020
 */
class MainActivityTest {

    private val dummyMovie = DataMoviesRemote.movies()
    private val dummyTvShow = DataMoviesRemote.tvshow()
    private val dummyPopular = DataMoviesRemote.popular()
    private val dummyNewest = DataMoviesRemote.newest()

    @Before
    fun setup() {
        ActivityScenario.launch(MainActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
        IdlingPolicies.setMasterPolicyTimeout(120, TimeUnit.SECONDS)
        IdlingPolicies.setIdlingResourceTimeout(90, TimeUnit.SECONDS)
    }

    @Test
    fun loadCategoryHome() {
        onView(withId(R.id.rvCategory)).perform(NestedScrollViewExtension())
        onView(withId(R.id.rvCategory)).check(matches(isDisplayed()))
    }

    @Test
    fun loadMoviesByCategoryAndLoadDetail() {
        onView(withId(R.id.rvCategory)).perform(NestedScrollViewExtension())
        onView(withId(R.id.rvCategory)).check(matches(isDisplayed()))
        onView(withId(R.id.rvCategory)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        onView(withId(R.id.rvMovies)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click()))
        assertDetail(dummyMovie[2])
        Espresso.pressBack()
        Espresso.pressBack()
        onView(withId(R.id.rvCategory)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        onView(allOf(withId(R.id.rvMovies), isDisplayed())).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        assertDetail(dummyTvShow[1])
    }

    @Test
    fun loadPopularMoviesAndLoadDetail() {
        onView(withId(R.id.rvPopularMovies)).perform(NestedScrollViewExtension())
        onView(withId(R.id.rvPopularMovies)).check(matches(isDisplayed()))
        onView(withId(R.id.rvPopularMovies)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(dummyPopular.size - 1))
        onView(withId(R.id.rvPopularMovies)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        assertDetail(dummyPopular[1])
    }

    @Test
    fun loadNewestMoviesAndLoadDetail() {
        onView(withId(R.id.rvNewestMovies)).perform(NestedScrollViewExtension())
        onView(withId(R.id.rvNewestMovies)).check(matches(isDisplayed()))
        onView(withId(R.id.rvNewestMovies)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(dummyNewest.size - 1))
        onView(withId(R.id.rvNewestMovies)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        assertDetail(dummyNewest[0])
    }

    @Test
    fun loadSavedMovies() {
        onView(withId(R.id.rvPopularMovies)).perform(NestedScrollViewExtension())
        onView(withId(R.id.rvPopularMovies)).check(matches(isDisplayed()))
        onView(withId(R.id.rvPopularMovies)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(dummyPopular.size - 1))
        onView(withId(R.id.rvPopularMovies)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        onView(withId(R.id.llBookmarkDetail)).check(matches(isDisplayed()))
        onView(withId(R.id.llBookmarkDetail)).perform(click())
        Espresso.pressBack()
        onView(withId(R.id.rvBottomMenu)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        onView(withId(R.id.rvSavedMovie)).check(matches(isDisplayed()))
        onView(withId(R.id.rvSavedMovie)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        onView(withId(R.id.llBookmarkDetailSaved)).check(matches(isDisplayed()))
        onView(withId(R.id.llBookmarkDetailSaved)).perform(click())
        onView(withText(R.string.title_delete_favorite_confirm)).check(matches(isDisplayed()))
        onView(withText("YES")).check(matches(isDisplayed()))
        onView(withText("YES")).perform(click())
        onView(withId(R.id.llEmpty)).check(matches(isDisplayed()))
    }

    @Test
    fun loadProfile() {
        onView(withId(R.id.rvBottomMenu)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click()))
        onView(allOf(withId(R.id.tvAccountName))).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.tvAccountName))).check(matches(withText(R.string.title_username)))
        onView(allOf(withId(R.id.tvAccountJob))).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.tvAccountJob))).check(matches(withText(R.string.title_job)))
        onView(withId(R.id.rvAccountMenu)).perform(NestedScrollViewExtension())
        onView(allOf(withId(R.id.rvAccountMenu))).check(matches(isDisplayed()))
    }

    private fun assertDetail(movies: MoviesRemote) {
        onView(allOf(withId(R.id.ivPoster))).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.ivSaved))).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.tvRating))).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.tvRating))).check(matches(withText(movies.rating)))
        onView(allOf(withId(R.id.tvTitleMovie))).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.tvTitleMovie))).check(matches(withText(movies.title)))
        onView(allOf(withId(R.id.tvGenre))).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.tvGenre))).check(matches(withText(movies.genre)))
        onView(allOf(withId(R.id.tvTime))).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.tvTime))).check(matches(withText(movies.time)))
        onView(withId(R.id.rvCast)).perform(NestedScrollViewExtension())
        onView(allOf(withId(R.id.tvSummary))).check(matches(isDisplayed()))
        onView(allOf(withId(R.id.tvSummary))).check(matches(withText(movies.summary)))
        onView(allOf(withId(R.id.rvCast))).check(matches(isDisplayed()))
    }

    inner class NestedScrollViewExtension(scrolltoAction: ViewAction = scrollTo()) : ViewAction by scrolltoAction {
        override fun getConstraints(): Matcher<View> {
            return Matchers.allOf(withEffectiveVisibility(Visibility.VISIBLE),
                    isDescendantOfA(Matchers.anyOf(isAssignableFrom(NestedScrollView::class.java),
                            isAssignableFrom(ScrollView::class.java),
                            isAssignableFrom(HorizontalScrollView::class.java),
                            isAssignableFrom(ListView::class.java))))
        }
    }
}